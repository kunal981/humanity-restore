var win = Titanium.UI.createWindow({
    width:"100%", height:"100%",
    
    backgroundColor:'white'
});

/*var ranvgeview = Titanium.UI.createView({
    width:300, height:40,
    backgroundColor:'black',
    top:50 , left:10
});
win.add(ranvgeview);

var min1 = Titanium.UI.createView({
    width:"30", height:40,
    backgroundColor:'#CCC',
    top:0 , left:0
});
ranvgeview.add(min1);
 
var min2 = Titanium.UI.createView({
    width:"30", height:40,
    top:0 , left:0
});
ranvgeview.add(min2);

var max1 = Titanium.UI.createView({
    width:"30", height:40,
    backgroundColor:'#CCC',
    top:0 , left:300-30
});
ranvgeview.add(max1);
 
var max2 = Titanium.UI.createView({
    width:"30", height:40,
    top:0 , left:300-30
});
ranvgeview.add(max2);

var offsetmin = {};
var offsetmax = {};
 
min2.addEventListener('touchstart', function(e){
    offsetmin.x = e.x - min1.left;
    offsetmin.y = 0;
    
    //label.text=offsetmin.x;
});
 
max2.addEventListener('touchstart', function(e){
    offsetmax.x = e.x - max1.left;
    offsetmax.y = 0;
    
    //label.text=offsetmax.x;
});

 
min2.addEventListener('touchmove', function(e){
    
    if(min1.left>=0 && min1.left<=(max1.left-30))
    {
    	min1.left = e.x - offsetmin.x;
    	min1.top = 0;
    }
    else
    {
	    if((min1.left)<0)
	    {
	    	min1.left = 0;
	    	min1.top = 0;
	    }
	    
	    if((min1.left)>(max1.left-30))
	    {
	    	min1.left = (max1.left-30);
	    	min1.top = 0;
	    }
    }
    label.text="min:"+parseInt(min1.left/30)+" max:"+parseInt(max1.left/30);
});


max2.addEventListener('touchmove', function(e){
    
    if(max1.left>=(min1.left+30) && max1.left<=(300-30))
    {
    	max1.left = e.x - offsetmax.x;
    	max1.top = 0;
    }
    else
    {
	    if((max1.left)<(min1.left+30))
	    {
	    	max1.left = (min1.left+30);
	    	max1.top = 0;
	    }
	    
	    if((max1.left)>(300-30))
	    {
	    	max1.left = (300-30);
	    	max1.top = 0;
	    }
    }
    label.text="min:"+parseInt(min1.left/30)+" max:"+parseInt(max1.left/30);

});
 
min2.addEventListener('touchend', function(e){
	if((min1.left)<0)
	{
	 	min1.left = 0;
	 	min1.top = 0;
	}
	    
	if((min1.left)>(max1.left-30))
	{
	 	min1.left = (max1.left-30);
		min1.top = 0;
	}
	min2.left=min1.left;
    min2.top = 0;
    label.text="min:"+parseInt(min1.left/30)+" max:"+parseInt(max1.left/30);
});

max2.addEventListener('touchend', function(e){
	if((max1.left)<(min1.left+30))
	{
		max1.left = (min1.left+30);
		max1.top = 0;
	}
	    
	if((max1.left)>(300-30))
	{
		max1.left = (300-30);
		max1.top = 0;
	}
	max2.left=max1.left;
    max2.top = 0;
    label.text="min:"+parseInt(min1.left/30)+" max:"+parseInt(max1.left/30);
});


var label = Titanium.UI.createLabel({
    height:40    
});
 
win.add(label);*/

var Paypal = require('ti.paypal');

//var win = Ti.UI.currentWindow;
var u = Ti.Android != undefined ? 'dp' : 0;

var status = Ti.UI.createLabel({
    top: 50 + u, height: 45 + u, color: '#333',
    text: 'Loading, please wait...'
});
win.add(status);

var button;
function addButtonToWindow() {
    if (button) {
        win.remove(button);
        button = null;
    }
    button = Paypal.createPaypalButton({
        // NOTE: height/width only determine the size of the view that the button is embedded in - the actual button size
        // is determined by the buttonStyle property!
        width: 194 + u, height: 37 + u,
        buttonStyle: Paypal.BUTTON_194x37, // The style & size of the button
        //bottom: 50 + u,

        language: 'en_US',
        textStyle: Paypal.PAYPAL_TEXT_DONATE, // Causes the button's text to change from "Pay" to "Donate"

        appID: 'APP_80W294485PS19543T', // The appID issued by Paypal for your application; for testing, feel free to delete this property entirely.
        paypalEnvironment: Paypal.PAYPAL_ENV_SANDBOX, // Sandbox, None or Live

        feePaidByReceiver: false,
        enableShipping: false, // Whether or not to select/send shipping information

        payment: { // The payment itself
			paymentType: Paypal.PAYMENT_TYPE_SERVICE, // The type of payment
			amount : 12.00,
			tax : 0.00, // The type of payment
			shipping : 0.00, // The subtype of the payment; you must be authorized for this by Paypal!
			currency : 'USD',
			recipient : 'dimpal1990-facilitator@gmail.com',
			//customID : 'rahul5040@gmail.com',
			itemDescription : "donation",
			merchantName : "Dev Tools"
        }
    });

    // Events available
    button.addEventListener('paymentCancelled', function (e) {
        status.text = 'Payment Cancelled.';
        // The button should only be used once; so after a payment is cancelled, succeeds, or errors, we must redisplay it.
        addButtonToWindow();
    });
    button.addEventListener('paymentSuccess', function (e) {
        status.text = 'Payment Success.  TransactionID: ' + e.transactionID + ', Reloading...';
        // The button should only be used once; so after a payment is cancelled, succeeds, or errors, we must redisplay it.
        addButtonToWindow();
    });
    button.addEventListener('paymentError', function (e) {
        status.text = 'Payment Error,  errorCode: ' + e.errorCode + ', errorMessage: ' + e.errorMessage;
        // The button should only be used once; so after a payment is cancelled, succeeds, or errors, we must redisplay it.
        addButtonToWindow();
    });

    button.addEventListener('buttonDisplayed', function () {
        status.text = 'The button was displayed!';
    });
    button.addEventListener('buttonError', function () {
        status.text = 'The button failed to display!';
    });
    win.add(button);
}
addButtonToWindow();

win.open();
