function FirstView() {

	var font1 = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
	var font2 = (Titanium.Platform.displayCaps.platformHeight * 2.5) / 100;
	var font3 = (Titanium.Platform.displayCaps.platformHeight * 2.2) / 100;
	var font4 = (Titanium.Platform.displayCaps.platformHeight * 1.8) / 100;
	var font5 = (Titanium.Platform.displayCaps.platformHeight * 1.5) / 100;
	var border = (Titanium.Platform.displayCaps.platformHeight * 0.5) / 100;
	var hgt = Ti.Platform.displayCaps.platformHeight;
	var wdh = Ti.Platform.displayCaps.platformWidth;

	var Allview = Ti.UI.createView({
		top : 0,
		backgroundColor : '#F3F3F3',
		height : '100%',
		width : '100%',
		animated : false,
		transition : Titanium.UI.iPhone.AnimationStyle.NONE,
	});

	var indicatorView = Ti.UI.createView({
		backgroundColor : 'black',
		height : '25%',
		width : '40%',
		opacity : 0.7,
		borderRadius : 10
	});

	var activityIndicator = Ti.UI.createActivityIndicator({
		style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,

	});
	indicatorView.add(activityIndicator);

	var topView = Ti.UI.createView({
		backgroundImage : '/images/header_bg.png',
		backgroundColor : '#5F6465',
		height : '10%',
		width : '100%',
		top : 0
	});

	Allview.add(topView);

	// Create an ImageView.
	var aboutimages = Ti.UI.createImageView({
		image : '/images/about-2.png',
		width : '12%',
		height : '70%',
		left : '2%'
	});
	aboutimages.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});

	// Add to the parent view.
	topView.add(aboutimages);

	// Create a Button.
	var Login = Ti.UI.createButton({
		backgroundImage : '/images/login_another.png',
		style : 'none',
		font : {
			fontSize : 12,
			fontWeight : 'bold',
		},
		color : 'white',
		title : 'Login',
		height : '70%',
		width : '20%',
		right : '2%',
		borderRadius : border
	});
	Login.addEventListener('touchstart', function(e) {

		Login.backgroundImage = '/images/login_hover.png';

	});
	Login.addEventListener('touchcancel', function(e) {

		Login.backgroundImage = '/images/login_another.png';

	});
	Login.addEventListener('touchend', function(e) {

		Login.backgroundImage = '/images/login_another.png';

	});

	// Listen for click events.
	Login.addEventListener('click', function() {
		alert('Login Screen');
	});

	// Add to the parent view.
	topView.add(Login);

	// Create an ImageView.
	var iconTop = Ti.UI.createImageView({
		image : '/images/icon.png',
		width : '15%',
		height : '80%',
		left : '42%'
	});
	iconTop.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});

	// Add to the parent view.
	topView.add(iconTop);

	var productView = Ti.UI.createView({
		backgroundColor : '#EDEDED',
		top : '10%',
		height : '55%',
		width : '100%'
	});

	Allview.add(productView);

	// Create an ImageView.
	var mainimage = Ti.UI.createImageView({
		image : '/images/magento.png',
		width : '96%',
		height : '94%',
	});
	mainimage.addEventListener('load', function() {
	});

	// Add to the parent view.
	productView.add(mainimage);

	first();
	function first() {

		Allview.add(indicatorView);
		activityIndicator.show();

		var all_name = [];
		var _image_url_ = [];
		var url = "http://siliwi.com/index.php/apiinfo/index/categorylisting";
		var xhr = Ti.Network.createHTTPClient({
			onload : function(e) {

				var json = JSON.parse(this.responseText);

				for (var i = 0; i < json.result.length; i++) {

					all_name.push(json.result[i].name);
					_image_url_.push(json.result[i].image_url);

				}
				Ti.API.info("????????????????????" + all_name);
				Ti.API.error(all_name.length);

				var count = 0;
				var productscroll = Ti.UI.createScrollView({
					contentWidth : 'auto',
					contentHeight : 'auto',
					top : '64%',
					height : '28%',
					width : '100%',
					scrollType : 'horizontal',
					backgroundColor : '#E89F4F',
				});

				Allview.add(productscroll);
				var view = Ti.UI.createView({
					backgroundColor : '#E89F4F',
					height : '90%',
					width : 940,
					layout : 'horizontal'
				});
				productscroll.add(view);

				for (var i = 0; i < all_name.length; i++) {

					var data = Ti.UI.createView({
						left : wdh * 0.04,
						height : '100%',
						width : wdh * 0.28,
						backgroundColor : 'white',
					});
					view.add(data);

					//	Create an ImageView.
					var smallimage1 = Ti.UI.createImageView({
						image : _image_url_[count],
						width : '90%',
						height : '75%',
						top : 2,
					});

					// Add to the parent view.
					data.add(smallimage1);

					var Button1 = Ti.UI.createButton({
						backgroundImage : '/images/backimage_button.png',
						//backgroundColor : '#D18B00',
						title : all_name[count],
						height : '15%',
						color : 'white',
						font : {
							fontSize : font4
						},
						//style:none,
						width : '100%',
						bottom : '2%',
						borderRadius : border
					});

					// Listen for click events.
					Button1.addEventListener('click', function(e) {
						mainimage.image = '/images/magento.png';
					});

					// Add to the parent view.
					data.add(Button1);
					count++;
					if (count === all_name.length) {
						count = 0;
					}
				}
				activityIndicator.hide();
				Allview.remove(indicatorView);
			},

			onerror : function(e) {
				activityIndicator.hide();
				Allview.remove(indicatorView);
				Ti.API.debug(e.error);
				alert('Server not respond');
			},
			timeout : 35000
		});

		xhr.open("GET", url);
		xhr.send();

	}

	//*************************************************************************Bottom Tabs************************************************************************

	var bottomview = Ti.UI.createView({
		backgroundImage : '/images/footer_bg.png',
		backgroundColor : '#363D40',
		bottom : 0,
		height : '10%',
		width : '100%',
		layout : 'horizontal'
	});
	Allview.add(bottomview);

	var HomeTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	HomeTab.addEventListener('click', function() {
		var Home = require('/ui/common/Home');
		var home = new Home();
		Allview.add(home);
		homeicon.image = '/images/home_hover.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = 'white';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(HomeTab);

	// Create an ImageView.
	var homeicon = Ti.UI.createImageView({
		image : '/images/home_hover.png',
		width : '40%',
		height : '50%',
		top : '13%'
	});

	// Add to the parent view.
	HomeTab.add(homeicon);

	// Create a Label.
	var Homelabel = Ti.UI.createLabel({
		text : 'HOME',
		color : 'white',
		font : {
			fontSize : font5
		},
		bottom : '0%',
		textAlign : 'center'
	});

	// Add to the parent view.
	HomeTab.add(Homelabel);

	var AccountTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	AccountTab.addEventListener('click', function() {
		var Account = require('/ui/common/Sales');
		var account = new Account();
		Allview.add(account);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user_hover.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = 'white';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(AccountTab);

	// Create an ImageView.
	var Accounticon = Ti.UI.createImageView({
		image : '/images/user.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	AccountTab.add(Accounticon);

	// Create a Label.
	var Accountlabel = Ti.UI.createLabel({
		text : 'ACCOUNT',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	AccountTab.add(Accountlabel);

	var ShopTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	ShopTab.addEventListener('click', function() {
		var Shop = require('/ui/common/Shop');
		var shop = new Shop();
		Allview.add(shop);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye_hover.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = 'white';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';
	});
	bottomview.add(ShopTab);

	// Create an ImageView.
	var Shopicon = Ti.UI.createImageView({
		image : '/images/eye.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	ShopTab.add(Shopicon);

	// Create a Label.
	var Shoplabel = Ti.UI.createLabel({
		text : 'SHOP',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	ShopTab.add(Shoplabel);

	var CartTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	CartTab.addEventListener('click', function() {
		var Cart = require('/ui/common/Cart');
		var cart = new Cart();
		Allview.add(cart);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart_hover.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = 'white';
		Morelabel.color = '#838383';
	});
	bottomview.add(CartTab);

	// Create an ImageView.
	var Carticon = Ti.UI.createImageView({
		image : '/images/cart.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	CartTab.add(Carticon);

	// Create a Label.
	var Cartlabel = Ti.UI.createLabel({
		text : 'CART',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	CartTab.add(Cartlabel);

	var MoreTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	MoreTab.addEventListener('click', function() {
		var More = require('/ui/common/More');
		var more = new More();
		Allview.add(more);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more_hover.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = 'white';
	});
	bottomview.add(MoreTab);

	// Create an ImageView.
	var Moreicon = Ti.UI.createImageView({
		image : '/images/more.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	MoreTab.add(Moreicon);

	// Create a Label.
	var Morelabel = Ti.UI.createLabel({
		text : 'MORE',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	MoreTab.add(Morelabel);

	//********************************************************************End Bottom Tab*************************************************************

	return Allview;
}

module.exports = FirstView;
