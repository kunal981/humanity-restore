var edit_screen = Ti.UI.currentWindow;

var hgt = Titanium.Platform.displayCaps.platformHeight;
var wdh = Ti.Platform.displayCaps.platformWidth;
var font1 = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
var font2 = (Titanium.Platform.displayCaps.platformHeight * 2.2) / 100;
var font3 = (Titanium.Platform.displayCaps.platformHeight * 2) / 100;
var font4 = (Titanium.Platform.displayCaps.platformHeight * 2.5) / 100;
var font5 = (Titanium.Platform.displayCaps.platformHeight * 1.5) / 100;

var center_view_;
var top_view;
var shopbutton;
var cartbutton;
var update_cart;
var tableData = [];
var thisRow = [];
var quantityEditText = [];
var myDatabase;
var _product_id = [];
var name_product = [];
var _quatity_ = [];
var _price_ = [];
var image_path = [];
var tableview;
var temp_value;
var prod = [];
var text = [];

myDatabase = Ti.Database.install('/mydata/ProductInformationSave', 'ProductInformationSave');
var get_info_database = myDatabase.execute('SELECT * from product_details');

while (get_info_database.isValidRow()) {
	Ti.API.error('enter edit_screen');
	_product_id.push(get_info_database.fieldByName('_product_id'));
	_quatity_.push(get_info_database.fieldByName('_quatity'));
	_price_.push(get_info_database.fieldByName('_single_price'));
	image_path.push(get_info_database.fieldByName('_image_path'));
	name_product.push(get_info_database.fieldByName('name_product'));

	get_info_database.next();
}

get_info_database.close();
myDatabase.close();
Ti.API.error('_quatity_=====' + _quatity_);
Ti.API.error('_price_=====' + _price_);
Ti.API.error('image_path=====' + image_path);
Ti.API.error('name_product=====' + name_product);

Ti.API.error("_quatity_" + _quatity_);
Ti.API.error("_price_" + _price_);

fetch_data();

function fetch_data() {

	top_view = Ti.UI.createView({

		backgroundImage : '/images/header_bg.png',

		height : '10%',
		width : '100%',
		top : '0%'

	});
	edit_screen.add(top_view);

	var indicatorView = Ti.UI.createView({
		backgroundColor : 'black',
		height : '25%',
		width : '40%',
		opacity : 0.7,

		borderRadius : 10
	});

	var activityIndicator = Ti.UI.createActivityIndicator({
		style : Ti.UI.ActivityIndicatorStyle.BIG_DARK,

	});
	indicatorView.add(activityIndicator);

	shopbutton = Ti.UI.createButton({
		backgroundImage : '/images/backimage_button.png',
		font : {
			fontSize : 14,
			fontWeight : 'bold',
		},
		color : 'white',

		height : '55%',
		width : '35%',
		style : 'none',

		title : 'Clear Cart',
		color : 'white',

		left : '2%',

	});

	/*
	shopbutton.addEventListener('touchstart', function(e) {

	shopbutton.backgroundImage = '/images/login_hover.png';

	});
	shopbutton.addEventListener('touchcancel', function(e) {

	shopbutton.backgroundImage = '/images/login_another.png';

	});
	shopbutton.addEventListener('touchend', function(e) {

	shopbutton.backgroundImage = '/images/login_another.png';

	});*/

	// Listen for click events.
	shopbutton.addEventListener('click', function() {
		/*
		 var Home = require('Cart');
		 var home = new Home();
		 edit_screen.add(home);*/

		var myDatabase;
		myDatabase = Ti.Database.install('/mydata/ProductInformationSave', 'ProductInformationSave');
		myDatabase.execute('DELETE FROM product_details');
		myDatabase.close();
		tableData = [];
		tableview.setData(tableData);

		edit_screen.add(indicatorView);
		activityIndicator.show();
		setTimeout(function() {

			activityIndicator.hide();
			edit_screen.remove(indicatorView);
			var Home = require('Cart');
			var home = new Home();
			edit_screen.add(home);
		}, 2000);

	});

	// Add to the parent view.
	top_view.add(shopbutton);

	update_cart = Ti.UI.createButton({
		title : 'Update Cart',
		backgroundImage : '/images/backimage_button.png',
		font : {
			fontSize : 14,
			fontWeight : 'bold',
		},
		color : 'white',

		height : '55%',
		width : '35%',
		right : '2%',

	});

	// Listen for click events.
	update_cart.addEventListener('click', function() {

		edit_screen.add(indicatorView);
		activityIndicator.show();
		setTimeout(function() {

			activityIndicator.hide();
			edit_screen.remove(indicatorView);
		}, 2000);

		for (var i = 0; i < prod.length; i++) {

			myDatabase = Ti.Database.install('/mydata/ProductInformationSave', 'ProductInformationSave');
			myDatabase.execute('UPDATE product_details SET _quatity=? WHERE _product_id=?', text[i], prod[i]);
			myDatabase.close();

		}
		prod = [];
		text = [];
		Ti.API.error("prod.length" + prod.length);
		Ti.API.error("text.length" + text.length);

		//top_view.remove(update_cart);
		//top_view.add(cartbutton);
		edit_screen.close();
		/*
		 var Home = require('Cart');
		 var home = new Home();
		 edit_screen.add(home);*/

	});
	top_view.add(update_cart);

	center_view_ = Ti.UI.createView({

		backgroundColor : '#C3C3C3',
		width : '100%',
		height : '90%',
		top : '10%',

	});
	edit_screen.add(center_view_);

	// create  view==========================================*************************************************************
	var colorSetIndex = 0;
	var cellIndex = 0;

	var repeatGetProductView;

	for (var j = 0; j < name_product.length; j++) {

		thisRow[j] = Ti.UI.createTableViewRow({
			//layout : "vertical",
			height : Ti.UI.SIZE,

			width : '100%',

			selectedColor : '#E89F4F',
			backgroundColor : '#D3D3D3',
			backgroundFocusedColor : '#E89F4F',
			focusable : true,
			backgroundSelectedColor : '#E89F4F',

		});
		/*
		 thisRow[j].addEventListener('touchstart', function(e) {

		 });
		 thisRow[j].addEventListener('touchcancel', function(e) {
		 //	e.source.setBackgroundColor('white');
		 thisView[(e.source.id - 1)].setBackgroundColor('white');
		 // just for effect
		 });
		 thisRow[j].addEventListener('touchend', function(e) {
		 //	e.source.setBackgroundColor('white');
		 thisView[(e.source.id - 1)].setBackgroundColor('white');
		 // just for effect
		 });*/

		thisRow[j].addEventListener('click', function(e) {
			/*
			 var quatity=[];
			 myDatabase = Ti.Database.install('/mydata/ProductInformationSave', 'ProductInformationSave');
			 var get_info_database = myDatabase.execute('SELECT * from product_details');

			 while (get_info_database.isValidRow()) {
			 Ti.API.error('enter edit_screen');

			 quatity.push(get_info_database.fieldByName('_quatity'));

			 get_info_database.next();
			 }

			 get_info_database.close();
			 myDatabase.close();

			 var temp = e.index;
			 var prod = _product_id[temp];

			 //alert(_product_id[temp]);_quatity_
			 var textfield = Ti.UI.createTextField({
			 keyboardType : Titanium.UI.KEYBOARD_NUMBER_PAD,
			 returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
			 borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
			 });

			 textfield.value = quatity[temp];
			 var dialog = Ti.UI.createAlertDialog({
			 title : 'Enter text',
			 cancel : 1,
			 androidView : textfield,
			 buttonNames : ['OK', 'cancel']
			 });
			 dialog.addEventListener('click', function(e) {
			 if (e.index == 0) {
			 var myDatabase;
			 myDatabase = Ti.Database.install('/mydata/ProductInformationSave', 'ProductInformationSave');
			 myDatabase.execute('UPDATE product_details SET _quatity=? WHERE _product_id=?', textfield.value, prod);
			 myDatabase.close();
			 quantityEditText[temp].value = textfield.value;

			 } else if (e.index == 1) {

			 }

			 });
			 dialog.show();
			 */

		});

		repeatGetProductView = Ti.UI.createView({
			height : Ti.UI.SIZE,
			width : '100%',
			layout : 'horizontal'
		});
		thisRow[j].add(repeatGetProductView);

		quantityEditText[j] = Ti.UI.createTextField({
			value : _quatity_[j],
			height : '40%',
			width : '12%',
			left : '85%',
			right : '5%',
			top : '20%',

			font : {
				fontSize : font3,
			},
			editable : true,
			borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
			//softKeyboardOnFocus : Titanium.UI.Android.SOFT_KEYBOARD_SHOW_ON_FOCUS,
			keyboardType : Titanium.UI.KEYBOARD_NUMBER_PAD,

			returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
			//autocorrect : false,
			id : j
		});

		// Add to the parent view.
		repeatGetProductView.add(quantityEditText[j]);

		quantityEditText[j].addEventListener('change', function(e) {

			temp_value = e.source.id;
			//top_view.remove(cartbutton);
			//top_view.add(update_cart);
			prod.push(_product_id[temp_value]);
			text.push(quantityEditText[temp_value].value);

			//quantityEditText[e.source.id].setRequestFocusEnabled = false;
			//quantityEditText[e.source.id].setFocusable = false;
			//quantityEditText[e.source.id].borderStyle = 'none';
		});

		// Create an ImageView.
		var productImage = Ti.UI.createImageView({
			image : image_path[j],
			width : '40%',
			height : 70,
			bottom : '2',
			top : '1%',
			left : '1%',
		});
		productImage.addEventListener('load', function() {
			Ti.API.info('Image loaded!');
		});

		// Add to the parent view.
		repeatGetProductView.add(productImage);

		var detailView = Ti.UI.createView({
			height : hgt * 0.15,
			width : wdh * 0.45,
			layout : 'vertical',
			left : '5%',
			top : hgt * 0.025,
		});

		repeatGetProductView.add(detailView);

		// Create a Label.
		var productNamelabel = Ti.UI.createLabel({
			text : name_product[j],
			color : 'black',
			font : {
				fontSize : font2
			},
			left : '0%',

		});

		// Add to the parent view.
		detailView.add(productNamelabel);

		// Create a Label.
		var unitpricelabel = Ti.UI.createLabel({
			text : 'unitprice: ' + _price_[j],
			color : 'black',
			font : {
				fontSize : font3
			},
			left : '0%',
		});

		// Add to the parent view.
		detailView.add(unitpricelabel);

		// Create a TextField.

		tableData.push(thisRow[j]);
	}

	tableview = Ti.UI.createTableView({
		data : tableData,
		backgroundColor : 'transparent',
		separatorColor : 'transparent'

	});

	tableview.addEventListener("click", function(e) {
		if (e.source.objName) {
			Ti.API.info("---> " + e.source.objName + e.source.objIndex + " was clicked!");
		}
	});
	Ti.API.error('====tableview=**************************************************************');

	Ti.API.error('====tableview=**************************************************************' + tableData.length);
	center_view_.add(tableview);
}

