function ProductListing() {

	var font1 = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
	var font2 = (Titanium.Platform.displayCaps.platformHeight * 2.5) / 100;
	var font3 = (Titanium.Platform.displayCaps.platformHeight * 2) / 100;
	var font4 = (Titanium.Platform.displayCaps.platformHeight * 1.8) / 100;
	var font5 = (Titanium.Platform.displayCaps.platformHeight * 1.5) / 100;

	var border = (Titanium.Platform.displayCaps.platformHeight * 0.5) / 100;
	var border2 = (Titanium.Platform.displayCaps.platformHeight * 0.8) / 100;

	var hgt = Ti.Platform.displayCaps.platformHeight;
	var wdh = Ti.Platform.displayCaps.platformWidth;

	var _id_cat_ = Ti.App.Properties.getString('cat_id_');
	Ti.App.Properties.setString('cat_id_', null);

	var Allview = Ti.UI.createView({
		top : 0,
		backgroundColor : '#E73A2F',
		height : '100%',
		width : '100%'
	});

	var indicatorView = Ti.UI.createView({
		backgroundColor : 'black',
		height : '25%',
		width : '40%',
		opacity : 0.7,
		borderRadius : 10
	});
	// code is changed iphone/android

	var activityIndicator = Ti.UI.createActivityIndicator({
		style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,

	});
	indicatorView.add(activityIndicator);

	var topView = Ti.UI.createView({
		backgroundImage : '/images/header_bg.png',
		backgroundColor : '#5F6465',
		height : '10%',
		width : '100%',
		top : 0
	});

	Allview.add(topView);

	// Create a Button.
	var shopbutton = Ti.UI.createButton({
		backgroundImage : '/images/back.png',
		title : ' Men',
		color : 'white',
		
		font : {
			fontSize : 14
		},
		style : 'none',
		height : '55%',
		width : '17%',
		left : '0.8%',
		borderRadius : border
	});

	shopbutton.addEventListener('touchstart', function(e) {

		shopbutton.backgroundImage = '/images/back_hover.png';

	});
	shopbutton.addEventListener('touchcancel', function(e) {

		shopbutton.backgroundImage = '/images/back.png';

	});
	shopbutton.addEventListener('touchend', function(e) {

		shopbutton.backgroundImage = '/images/back.png';

	});

	// Listen for click events.
	shopbutton.addEventListener('click', function() {
		Ti.UI.currentWindow.remove(Allview);
	});

	// Add to the parent view.
	topView.add(shopbutton);

	// Create a Label.
	var shoplabel = Ti.UI.createLabel({
		text : 'Hoodies',
		color : 'white',
		font : {
			fontSize : font1
		},

		textAlign : 'center'
	});

	// Add to the parent view.
	topView.add(shoplabel);

	var body = Ti.UI.createView({
		backgroundColor : '#F3F3F3',
		width : '100%',
		height : '90%',
		top : '10%'
	});
	Allview.add(body);

	var bodyTop = Ti.UI.createView({
		backgroundColor : '#B1B1B1',
		width : '100%',
		height : '10%',
		top : 0
	});
	body.add(bodyTop);

	// Create a Label.
	var shoplabel = Ti.UI.createLabel({
		text : 'Sort BY :',
		color : 'black',
		font : {
			fontSize : font3,
			fontweight : 'bold'
		},
		left : '5%',
		textAlign : 'center'
	});

	// Add to the parent view.
	bodyTop.add(shoplabel);

	var SortBy = Ti.UI.createView({
		backgroundColor : '#D5D5D5',
		width : '70%',
		height : '80%',
		right : '2%',
		borderColor : '#D5D5D5',
		borderRadius : 1,
		borderWidth : 3
	});
	bodyTop.add(SortBy);

	// Create a Button.
	var position = Ti.UI.createButton({
		backgroundColor : '#E76E01',
		title : 'Position',
		height : '94%',
		color : 'white',
		width : '32%',
		left : '1%'
	});

	// Listen for click events.
	position.addEventListener('click', function() {
		position.backgroundColor = '#E76E01';
		name.backgroundColor = '#8D8D8D';
		price.backgroundColor = '#8D8D8D';
	});

	// Add to the parent view.
	SortBy.add(position);

	// Create a Button.
	var name = Ti.UI.createButton({
		backgroundColor : '#8D8D8D',
		title : 'Name',
		color : 'white',
		height : '94%',
		width : '32.5%',
	});

	// Listen for click events.
	name.addEventListener('click', function() {
		position.backgroundColor = '#8D8D8D';
		name.backgroundColor = '#E76E01';
		price.backgroundColor = '#8D8D8D';
	});

	// Add to the parent view.
	SortBy.add(name);

	// Create a Button.
	var price = Ti.UI.createButton({
		backgroundColor : '#8D8D8D',
		title : 'Price',
		color : 'white',
		height : '94%',
		width : '32%',
		right : '1%'
	});

	// Listen for click events.
	price.addEventListener('click', function() {
		position.backgroundColor = '#8D8D8D';
		name.backgroundColor = '#8D8D8D';
		price.backgroundColor = '#E76E01';
	});

	// Add to the parent view.
	SortBy.add(price);

	first_time();
	//*************************************************************************table view start ******************************************************
	function first_time() {

		Allview.add(indicatorView);
		activityIndicator.show();
		var _url_thumbnail_ = [];
		var _name_ = [];
		var _price_ = [];
		var _id_ = [];
		var _special_price_ = [];
		var url1 = "http://siliwi.com/index.php/apiinfo/index/categoryproduct?catid=" + _id_cat_ + "?p=1&showproducts=10";
		var xhr = Ti.Network.createHTTPClient({
			onload : function(e) {

				var json = JSON.parse(this.responseText);
				Ti.API.info(this.responseText);

				for (var i = 0; i < json.result.length; i++) {

					_id_.push(json.result[i].id);
					_name_.push(json.result[i].name);
					_price_.push(json.result[i].price);
					_special_price_.push(json.result[i].SpecialPrice);
					_url_thumbnail_.push(json.result[i].thumbnail);
				}
				Ti.API.error("_id_========" + _id_);
				Ti.API.error("_id_========" + _id_.length);
				Ti.API.error("_name_========" + _name_);
				Ti.API.error("_price_========" + _price_);
				Ti.API.error("_special_price_========" + _special_price_);
				Ti.API.error("_url_thumbnail_========" + _url_thumbnail_);

				var tableData = [];
				var colorSetIndex = 0;
				var cellIndex = 0;

				var count = 1;

				var removetmp;

				var thisRow = [];
				var Viewquantity = [];
				var friend = [];
				var wishlist = [];
				var addcart = [];
				var showdetailview = [];
				var ProductDetailView = [];

				for (var y = 0; y < _id_.length; y++) {

					var tmp_click_row;

					thisRow[y] = Ti.UI.createTableViewRow({
						height : Ti.UI.SIZE,
						backgroundColor : '#D3D3D3',
						selectedBackgroundColor : 'transparent',
						selectedColor : 'transparent'

					});

					thisRow[y].addEventListener('touchstart', function(e) {
						//thisRow[(e.index)].setBackgroundColor('#D3D3D3');
					});
					thisRow[y].addEventListener('touchcancel', function(e) {
						//thisRow[(e.index)].setBackgroundColor('#D3D3D3');
					});
					thisRow[y].addEventListener('touchend', function(e) {
						//thisRow[(e.index)].setBackgroundColor('#D3D3D3');
					});

					thisRow[y].addEventListener('click', function(e) {
						if (removetmp != undefined) {
							thisRow[removetmp].remove(orangeView);
						}

						tmp_click_row = e.index;
						Ti.App.Properties.setString('tmp_click_row', tmp_click_row);
						thisRow[tmp_click_row].add(orangeView);
						removetmp = tmp_click_row;
					});

					/*
					 thisRow[y].addEventListener('swipe', function(e) {
					 var tmp = e.index;
					 if (e.direction == 'left') {
					 } else if (e.direction == 'right') {
					 Ti.App.Properties.setString('selected_item', 'Nokia Phone ' + (tmp + 1));
					 var ProductInformationPage = require('ProductInformationPage');
					 var productInformationPage = new ProductInformationPage();
					 Ti.UI.currentWindow.add(productInformationPage);
					 }
					 });*/

					ProductDetailView[y] = Ti.UI.createView({
						width : wdh,
						height : hgt * 0.2,
						top : 0,
						id : colorSetIndex
					});

					thisRow[y].add(ProductDetailView[y]);

					ProductDetailView[y].addEventListener('touchstart', function(e) {
						ProductDetailView[(e.source.id)].setBackgroundColor('#E89F4F');
					});
					ProductDetailView[y].addEventListener('touchcancel', function(e) {
						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
					});
					ProductDetailView[y].addEventListener('touchend', function(e) {
						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
					});

					ProductDetailView[y].addEventListener('click', function(e) {
						if (removetmp != undefined) {
							thisRow[removetmp].remove(orangeView);
						}
						tmp_click_row = e.source.id;
						ProductDetailView[tmp_click_row].add(orangeView);
						removetmp = tmp_click_row;
					});

					// Create an ImageView.
					var productImage = Ti.UI.createImageView({
						image : _url_thumbnail_[colorSetIndex],
						width : wdh * 0.3,
						height : hgt * 0.15,
						left : wdh * 0.02,
						id : colorSetIndex,
						borderColor : 'white',
						borderWidth : 3
					});
					productImage.addEventListener('load', function() {
						Ti.API.info('Image loaded!');
					});

					// Add to the parent view.
					ProductDetailView[y].add(productImage);

					var detail = Ti.UI.createView({
						width : wdh * 0.5,
						height : hgt * 0.15,
						left : wdh * 0.35,
						layout : 'vertical',
						id : colorSetIndex
					});
					ProductDetailView[y].add(detail);

					// Create a Label.
					var categoryText1 = Ti.UI.createLabel({
						text : _name_[colorSetIndex],
						color : 'black',
						font : {
							fontSize : font2,
							fontWeight : 'bold',
						},
						left : 0,
						id : colorSetIndex
					});

					// Add to the parent view.
					detail.add(categoryText1);

					// Create a Label.
					var categoryText2 = Ti.UI.createLabel({
						text : 'Regular $' + _price_[colorSetIndex],
						color : 'black',
						font : {
							fontSize : font3
						},
						left : 0,
						id : colorSetIndex
					});

					// Add to the parent view.
					detail.add(categoryText2);

					if (_special_price_[colorSetIndex] == null) {

					} else {
						var categoryText3 = Ti.UI.createLabel({
							text : 'Special $' + _special_price_[colorSetIndex],
							color : 'black',
							font : {
								fontSize : font3
							},
							left : 0,
							id : colorSetIndex
						});

						// Add to the parent view.
						detail.add(categoryText3);
					}
					// Create a Label.

					/*
					// Create a Label.
					var categoryText4 = Ti.UI.createLabel({
					text : 'Out of stock',
					color : '#A8A8A8',
					font : {
					fontSize : font3
					},
					left : 0,
					id : colorSetIndex
					});

					// Add to the parent view.
					detail.add(categoryText4);
					*/

					// Create an ImageView.
					var arrow = Ti.UI.createImageView({
						image : '/images/arrow.png',
						width : wdh * 0.1,
						height : hgt * 0.05,
						right : wdh * 0.02,
						top : hgt * 0.07,
						id : colorSetIndex
					});
					arrow.addEventListener('load', function() {
						Ti.API.info('Image loaded!');
					});

					// Add to the parent view.
					ProductDetailView[y].add(arrow);

					var orangeView = Ti.UI.createView({
						backgroundColor : '#E76E01',
						width : wdh,
						height : hgt * 0.15,
						top : hgt * 0.2,
						layout : 'horizontal'
					});

					Viewquantity[y] = Ti.UI.createView({
						width : '20%',
						height : '80%',
						top : '10%',
						id : colorSetIndex
					});
					orangeView.add(Viewquantity[y]);
					Viewquantity[y].addEventListener('touchstart', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						Viewquantity[(e.source.id)].setBackgroundColor('#E89F4F');
					});
					Viewquantity[y].addEventListener('touchcancel', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						Viewquantity[(e.source.id)].setBackgroundColor('#E76E01');
					});
					Viewquantity[y].addEventListener('touchend', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						Viewquantity[(e.source.id)].setBackgroundColor('#E76E01');
					});
					Viewquantity[y].addEventListener('click', function(e) {

						var temp_view = e.source.id;
						alert('View=====' + temp_view);

					});

					// Create an ImageView.
					var quantityImage = Ti.UI.createImageView({
						image : '/images/gallery.png',
						width : '50%',
						height : '50%',
						top : '0%',
						id : colorSetIndex
					});
					quantityImage.addEventListener('load', function() {
						Ti.API.info('Image loaded!');
					});

					// Add to the parent view.
					Viewquantity[y].add(quantityImage);

					// Create a Label.
					var quantityLabel = Ti.UI.createLabel({
						text : 'View Quantity',
						color : 'white',
						font : {
							fontSize : font4,
							fontWeight : 'bold'
						},
						top : '60%',
						textAlign : 'center',
						left : '15%',
						right : '15%',
						id : colorSetIndex
					});

					// Add to the parent view.
					Viewquantity[y].add(quantityLabel);

					friend[y] = Ti.UI.createView({
						width : '20%',
						height : '80%',
						top : '10%',
						id : colorSetIndex
					});
					orangeView.add(friend[y]);

					friend[y].addEventListener('touchstart', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						friend[(e.source.id)].setBackgroundColor('#E89F4F');
					});
					friend[y].addEventListener('touchcancel', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						friend[(e.source.id)].setBackgroundColor('#E76E01');
					});
					friend[y].addEventListener('touchend', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						friend[(e.source.id)].setBackgroundColor('#E76E01');
					});
					friend[y].addEventListener('click', function(e) {
						var temp_frd = e.source.id;
						alert('freind====' + temp_frd);

					});

					// Create an ImageView.
					var friendImage = Ti.UI.createImageView({
						image : '/images/msg.png',
						width : '50%',
						height : '50%',
						top : '0%',
						id : colorSetIndex
					});
					friendImage.addEventListener('load', function() {
						Ti.API.info('Image loaded!');
					});

					// Add to the parent view.
					friend[y].add(friendImage);

					// Create a Label.
					var friendLabel = Ti.UI.createLabel({
						text : 'Tell a Friend',
						color : 'white',
						font : {
							fontSize : font4,
							fontWeight : 'bold'
						},
						top : '60%',
						textAlign : 'center',
						left : '20%',
						right : '20%',
						id : colorSetIndex
					});

					// Add to the parent view.
					friend[y].add(friendLabel);

					wishlist[y] = Ti.UI.createView({
						width : '20%',
						height : '80%',
						top : '10%',
						id : colorSetIndex
					});
					orangeView.add(wishlist[y]);

					wishlist[y].addEventListener('touchstart', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						wishlist[(e.source.id)].setBackgroundColor('#E89F4F');
					});
					wishlist[y].addEventListener('touchcancel', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						wishlist[(e.source.id)].setBackgroundColor('#E76E01');
					});
					wishlist[y].addEventListener('touchend', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						wishlist[(e.source.id)].setBackgroundColor('#E76E01');
					});
					wishlist[y].addEventListener('click', function(e) {
						var temp_wishlist = e.source.id;
						alert('wishlist===' + temp_wishlist);
						/*
						 Ti.App.Properties.setString('selected_item', 'Nokia Phone ' + (tmp_click_row + 1));
						 var ProductInformationPage = require('ProductInformationPage');
						 var productInformationPage = new ProductInformationPage();
						 Ti.UI.currentWindow.add(productInformationPage);*/

					});

					// Create an ImageView.
					var wishlistImage = Ti.UI.createImageView({
						image : '/images/star.png',
						width : '50%',
						height : '50%',
						top : '0%',
						id : colorSetIndex
					});
					wishlistImage.addEventListener('load', function() {
						Ti.API.info('Image loaded!');
					});

					// Add to the parent view.
					wishlist[y].add(wishlistImage);

					// Create a Label.
					var wishlistLabel = Ti.UI.createLabel({
						text : 'Add to Wishlist',
						color : 'white',
						font : {
							fontSize : font4,
							fontWeight : 'bold'
						},
						top : '60%',
						textAlign : 'center',
						left : '15%',
						right : '15%',
						id : colorSetIndex
					});

					// Add to the parent view.
					wishlist[y].add(wishlistLabel);

					addcart[y] = Ti.UI.createView({
						width : '20%',
						height : '80%',
						top : '10%',
						id : colorSetIndex
					});
					addcart[y].addEventListener('touchstart', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						addcart[(e.source.id)].setBackgroundColor('#E89F4F');
					});
					addcart[y].addEventListener('touchcancel', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						addcart[(e.source.id)].setBackgroundColor('#E76E01');
					});
					addcart[y].addEventListener('touchend', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						addcart[(e.source.id)].setBackgroundColor('#E76E01');
					});

					addcart[y].addEventListener('click', function(e) {
						var temp_add = e.source.id;
						//alert('Add to Cart=====' + temp_add);
						var aa = Ti.App.Properties.getString('tmp_click_row');
						Ti.App.Properties.setString('tmp_click_row', null);
						//alert(_name_[aa]+'-----------'+_url_thumbnail_[aa]);

						Ti.App.Properties.setString('_id_product', _id_[aa]);
						Ti.App.Properties.setString('_name_product', _name_[aa]);
						Ti.App.Properties.setString('_price_product', _price_[aa]);
						Ti.App.Properties.setString('_image_product', _url_thumbnail_[aa]);
						Ti.App.Properties.setString('selected_item', 'Nokia Phone ' + (tmp_click_row + 1));
						var ProductInformationPage = require('/ui/common/ProductInformationPage');
						var productInformationPage = new ProductInformationPage();
						Ti.UI.currentWindow.add(productInformationPage);

					});

					orangeView.add(addcart[y]);

					// Create an ImageView.
					var addcartImage = Ti.UI.createImageView({
						image : '/images/add_cart.png',
						width : '50%',
						height : '50%',
						top : '0%',
						id : colorSetIndex
					});
					addcartImage.addEventListener('load', function() {
						Ti.API.info('Image loaded!');
					});

					// Add to the parent view.
					addcart[y].add(addcartImage);

					// Create a Label.
					var addcartLabel = Ti.UI.createLabel({
						text : 'Add to Cart',
						color : 'white',
						font : {
							fontSize : font4,
							fontWeight : 'bold'
						},
						top : '60%',
						textAlign : 'center',
						left : '20%',
						right : '20%',
						id : colorSetIndex
					});

					// Add to the parent view.
					addcart[y].add(addcartLabel);

					showdetailview[y] = Ti.UI.createView({
						width : '20%',
						height : '80%',
						top : '10%',
						id : colorSetIndex
					});

					showdetailview[y].addEventListener('touchstart', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						showdetailview[(e.source.id)].setBackgroundColor('#E89F4F');
					});
					showdetailview[y].addEventListener('touchcancel', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						showdetailview[(e.source.id)].setBackgroundColor('#E76E01');
					});
					showdetailview[y].addEventListener('touchend', function(e) {

						ProductDetailView[(e.source.id)].setBackgroundColor('#D3D3D3');
						showdetailview[(e.source.id)].setBackgroundColor('#E76E01');
					});

					showdetailview[y].addEventListener('click', function(e) {
						var temp_view = e.source.id;
						alert('View Detail=====' + temp_view);
						/*
						 Ti.App.Properties.setString('selected_item', 'Nokia Phone ' + (tmp_click_row + 1));
						 var ProductInformationPage = require('ProductInformationPage');
						 var productInformationPage = new ProductInformationPage();
						 Ti.UI.currentWindow.add(productInformationPage);*/

					});
					orangeView.add(showdetailview[y]);

					// Create an ImageView.
					var showdetailImage = Ti.UI.createImageView({
						image : '/images/eye_cart.png',
						width : '50%',
						height : '50%',
						top : '0%',
						id : colorSetIndex
					});
					showdetailImage.addEventListener('load', function() {
						Ti.API.info('Image loaded!');
					});

					// Add to the parent view.
					showdetailview[y].add(showdetailImage);

					// Create a Label.
					var showdetailLabel = Ti.UI.createLabel({
						text : 'View Detail',
						color : 'white',
						font : {
							fontSize : font4,
							fontWeight : 'bold'
						},
						top : '60%',
						textAlign : 'center',
						left : '15%',
						right : '15%',
						id : colorSetIndex
					});

					// Add to the parent view.
					showdetailview[y].add(showdetailLabel);
					cellIndex++;
					colorSetIndex++;

					tableData.push(thisRow[y]);
					count++;

				}

				var tableview = Ti.UI.createTableView({
					top : '10%',
					data : tableData
				});

				tableview.addEventListener("click", function(e) {
					if (e.source.objName) {
						Ti.API.info("---> " + e.source.objName + e.source.objIndex + " was clicked!");
					}
				});

				body.add(tableview);

				activityIndicator.hide();
				Allview.remove(indicatorView);
			},
			onerror : function(e) {
				activityIndicator.hide();
				Allview.remove(indicatorView);
				Ti.API.debug(e.error);
				alert('Internet Connection low');
			},
			timeout : 25000
		});

		xhr.open("GET", url1);
		xhr.send();

	}

	//*************************************************************************end table view************************************************************************

	//*************************************************************************Bottom Tabs************************************************************************

	var bottomview = Ti.UI.createView({
		backgroundImage : '/images/footer_bg.png',
		backgroundColor : '#363D40',
		bottom : 0,
		height : '10%',
		width : '100%',
		layout : 'horizontal'
	});
	Allview.add(bottomview);

	var HomeTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	HomeTab.addEventListener('click', function() {
		var Home = require('/ui/common/Home');
		var home = new Home();
		Allview.add(home);
		homeicon.image = '/images/home_hover.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = 'white';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(HomeTab);

	// Create an ImageView.
	var homeicon = Ti.UI.createImageView({
		image : '/images/home.png',
		width : '40%',
		height : '50%',
		top : '13%'
	});

	// Add to the parent view.
	HomeTab.add(homeicon);

	// Create a Label.
	var Homelabel = Ti.UI.createLabel({
		text : 'HOME',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '0%',
		textAlign : 'center'
	});

	// Add to the parent view.
	HomeTab.add(Homelabel);

	var AccountTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	AccountTab.addEventListener('click', function() {
		var Account = require('/ui/common/Sales');
		var account = new Account();
		Allview.add(account);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user_hover.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = 'white';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(AccountTab);

	// Create an ImageView.
	var Accounticon = Ti.UI.createImageView({
		image : '/images/user.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	AccountTab.add(Accounticon);

	// Create a Label.
	var Accountlabel = Ti.UI.createLabel({
		text : 'ACCOUNT',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	AccountTab.add(Accountlabel);

	var ShopTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	ShopTab.addEventListener('click', function() {
		var Shop = require('/ui/common/Shop');
		var shop = new Shop();
		Allview.add(shop);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye_hover.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = 'white';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';
	});
	bottomview.add(ShopTab);

	// Create an ImageView.
	var Shopicon = Ti.UI.createImageView({
		image : '/images/eye_hover.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	ShopTab.add(Shopicon);

	// Create a Label.
	var Shoplabel = Ti.UI.createLabel({
		text : 'SHOP',
		color : 'white',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	ShopTab.add(Shoplabel);

	var CartTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	CartTab.addEventListener('click', function() {
		var Cart = require('/ui/common/Cart');
		var cart = new Cart();
		Allview.add(cart);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart_hover.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = 'white';
		Morelabel.color = '#838383';
	});
	bottomview.add(CartTab);

	// Create an ImageView.
	var Carticon = Ti.UI.createImageView({
		image : '/images/cart.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	CartTab.add(Carticon);

	// Create a Label.
	var Cartlabel = Ti.UI.createLabel({
		text : 'CART',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	CartTab.add(Cartlabel);

	var MoreTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	MoreTab.addEventListener('click', function() {
		var More = require('/ui/common/More');
		var more = new More();
		Allview.add(more);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more_hover.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = 'white';
	});
	bottomview.add(MoreTab);

	// Create an ImageView.
	var Moreicon = Ti.UI.createImageView({
		image : '/images/more.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	MoreTab.add(Moreicon);

	// Create a Label.
	var Morelabel = Ti.UI.createLabel({
		text : 'MORE',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	MoreTab.add(Morelabel);

	return Allview;
}

module.exports = ProductListing;
