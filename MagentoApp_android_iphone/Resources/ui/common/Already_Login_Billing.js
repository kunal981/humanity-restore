var alreadylogin_billing = Ti.UI.currentWindow;
var final_session_id = Ti.App.Properties.getString('session_id_');

var Allview = Ti.UI.createView({
	backgroundColor : '#F3F3F3',
	height : '100%',
	width : '100%',
	top : 0
});
alreadylogin_billing.add(Allview);
var indicatorView = Ti.UI.createView({
	backgroundColor : 'black',
	height : '25%',
	width : '40%',
	opacity : 0.7,
	borderRadius : 10
});

var activityIndicator = Ti.UI.createActivityIndicator({
	style : Ti.UI.ActivityIndicatorStyle.BIG_DARK,

});
indicatorView.add(activityIndicator);

var topView = Ti.UI.createView({
	backgroundImage : '/images/header_bg.png',
	backgroundColor : '#5F6465',
	height : '10%',
	width : '100%',
	top : 0
});

Allview.add(topView);

// Create a Button.
var aButton = Ti.UI.createButton({
	title : 'Back',
	backgroundImage : '/images/back.png',
	font : {
		fontSize : 13,
		fontWeight : 'bold',
	},
	color : 'white',

	height : '70%',
	width : '25%',
	left : '2%',
});

aButton.addEventListener('touchstart', function(e) {

	aButton.backgroundImage = '/images/back_hover.png';

});
aButton.addEventListener('touchcancel', function(e) {

	aButton.backgroundImage = '/images/back.png';

});
aButton.addEventListener('touchend', function(e) {

	aButton.backgroundImage = '/images/back.png';

});
aButton.addEventListener('click', function() {
	alreadylogin_billing.close();
});
topView.add(aButton);

// Create a Label.
var aLabel = Ti.UI.createLabel({
	text : 'STEP 1:Billing Information',
	color : '#ffffff',
	font : {
		fontSize : 15
	},
	height : 'auto',
	width : 'auto',

	textAlign : 'center'
});

// Add to the parent view.
topView.add(aLabel);
account_();

function account_() {

	var after_login_view = Ti.UI.createView({
		height : '80%',
		width : '100%',
		top : '10%',
		backgroundColor : 'white'
	});

	Allview.add(after_login_view);

	var json;
	var _city_;
	var country_id_;
	var region_;
	var postalcode_;
	var first;
	var last;
	var aLabel_firstname_;
	var aLabel_lastname;
	var aLabel_city;
	var aLabel_country;
	var aLabel_region;
	var aLabel_postalcode;

	Allview.add(indicatorView);
	activityIndicator.show();
	//http://siliwi.com/index.php/apiinfo/index/customerlogin?username=test.kumar@brihaspatitech.com&password=tester123
	Ti.API.error("?????????sdvzsdxcv???????????" + final_session_id);
	var url1 = "http://siliwi.com/index.php/apiinfo/index/fetchcustomeraddress?sessionid=" + final_session_id;

	var xhr = Ti.Network.createHTTPClient({
		onload : function(e) {

			Ti.API.debug(this.responseText);
			Ti.API.info(this.responseText);
			json = JSON.parse(this.responseText);
			Ti.API.info(json);

			//	for (var i = 0; i < json.result.length; i++) {
			first = json.result[0].firstname;
			last = json.result[0].lastname;
			_city_ = json.result[0].city;
			country_id_ = json.result[0].country_id;
			region_ = json.result[0].street;
			postalcode_ = json.result[0].postcode;
			//}
			Ti.API.info("????????????????????" + _city_);
			Ti.API.info("????????????????????" + country_id_);
			Ti.API.info("????????????????????" + region_);
			Ti.API.info("????????????????????" + postalcode_);

			// Create a Label.
			var aLabel_firstname = Ti.UI.createLabel({
				text : 'Firstname: ' + first,
				color : '#000000',
				font : {
					fontSize : 12
				},
				height : 'auto',
				width : 'auto',
				top : '2%',
				left : '10%',
				textAlign : 'center'
			});

			// Add to the parent view.
			after_login_view.add(aLabel_firstname);

			// Create a Label.
			var aLabel_lastname_ = Ti.UI.createLabel({
				text : 'LastName: ' + last,
				color : '#000000',
				font : {
					fontSize : 12
				},
				height : 'auto',
				width : 'auto',
				top : '17%',
				left : '10%',
				textAlign : 'center'
			});

			// Add to the parent view.
			after_login_view.add(aLabel_lastname_);

			var aLabel_city_ = Ti.UI.createLabel({
				text : 'City: ' + _city_,
				color : '#000000',
				font : {
					fontSize : 12
				},
				height : 'auto',
				width : 'auto',
				top : '31%',
				left : '10%',
				textAlign : 'center'
			});

			// Add to the parent view.
			after_login_view.add(aLabel_city_);

			var aLabel_country_ = Ti.UI.createLabel({
				text : 'Country: ' + country_id_,
				color : '#000000',
				font : {
					fontSize : 12
				},
				height : 'auto',
				width : 'auto',
				top : '45%',
				left : '10%',
				textAlign : 'center'
			});

			// Add to the parent view.
			after_login_view.add(aLabel_country_);

			var aLabel_street_ = Ti.UI.createLabel({
				text : 'Street: ' + region_,
				color : '#000000',
				font : {
					fontSize : 12
				},
				height : 'auto',
				width : 'auto',
				top : '58%',
				left : '10%',
				textAlign : 'center'
			});

			// Add to the parent view.
			after_login_view.add(aLabel_street_);

			var aLabel_postalcode_ = Ti.UI.createLabel({
				text : 'PostalCode: ' + postalcode_,
				color : '#000000',
				font : {
					fontSize : 12
				},
				height : 'auto',
				width : 'auto',
				top : '72%',
				left : '10%',
				textAlign : 'center'
			});

			// Add to the parent view.
			after_login_view.add(aLabel_postalcode_);

			activityIndicator.hide();
			Allview.remove(indicatorView);

		},
		onerror : function(e) {
			activityIndicator.hide();
			Allview.remove(indicatorView);
			Ti.API.debug(e.error);
			alert('Internet Connection low');
		},
		timeout : 20000
	});

	xhr.open("GET", url1);
	xhr.send();

	// Create a Button.
	var Checkout_Guest = Ti.UI.createButton({
		title : 'Continue',
		backgroundImage : '/images/backimage_button.png',
		style : 'none',
		font : {
			fontSize : 12,
			fontWeight : 'bold',
		},
		color : 'white',
		height : '10%',

		top : '86%',
		left : '10%',
		width : '80%',
		right : '10%',
	});

	// Listen for click events.
	Checkout_Guest.addEventListener('click', function() {
		var app = Ti.UI.createWindow({
			backgroundColor : 'white',
			url : 'ui/common/Already_Login_Shipping.js',
			navBarHidden : true,
			fullscreen : true,

		});
		app.open();

	});

	// Add to the parent view.
	after_login_view.add(Checkout_Guest);
	Checkout_Guest.addEventListener('touchstart', function(e) {

		Checkout_Guest.backgroundImage = '/images/backimage_hover_button.png';

	});
	Checkout_Guest.addEventListener('touchcancel', function(e) {

		Checkout_Guest.backgroundImage = '/images/backimage_button.png';

	});
	Checkout_Guest.addEventListener('touchend', function(e) {

		Checkout_Guest.backgroundImage = '/images/backimage_button.png';

	});

}