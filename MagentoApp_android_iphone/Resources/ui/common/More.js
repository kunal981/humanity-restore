function More() {

	var font1 = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
	var font2 = (Titanium.Platform.displayCaps.platformHeight * 2.5) / 100;
	var font3 = (Titanium.Platform.displayCaps.platformHeight * 2.2) / 100;
	var font4 = (Titanium.Platform.displayCaps.platformHeight * 2) / 100;
	var font5 = (Titanium.Platform.displayCaps.platformHeight * 1.5) / 100;
	var border = (Titanium.Platform.displayCaps.platformHeight * 0.5) / 100;

	var Allview = Ti.UI.createView({
		backgroundColor : '#F3F3F3',
		height : '100%',
		width : '100%',
		top : 0,
		animated : false,
		transition : Titanium.UI.iPhone.AnimationStyle.NONE,
	});

	var topView = Ti.UI.createView({
		backgroundImage : '/images/header_bg.png',
		backgroundColor : '#5F6465',
		height : '10%',
		width : '100%',
		top : 0
	});

	Allview.add(topView);

	// Create an ImageView.
	var iconTop = Ti.UI.createImageView({
		image : '/images/icon.png',
		width : '15%',
		height : '80%',
		left : '42%'
	});
	iconTop.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});

	// Add to the parent view.
	topView.add(iconTop);

	/*
	// Create a Label.
		var shoplabel = Ti.UI.createLabel({
			text : 'More',
			color : 'white',
			font : {
				fontSize : font1
			},
			left : '48%',
			textAlign : 'center'
		});
	
		// Add to the parent view.
		topView.add(shoplabel);*/
	
	//*************************************************************************Bottom Tabs************************************************************************

	var bottomview = Ti.UI.createView({
		backgroundImage : '/images/footer_bg.png',
		backgroundColor : '#363D40',
		bottom : 0,
		height : '10%',
		width : '100%',
		layout : 'horizontal'
	});
	Allview.add(bottomview);

	var HomeTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	HomeTab.addEventListener('click', function() {
		var Home = require('/ui/common/Home');
		var home = new Home();
		Allview.add(home);
		homeicon.image = '/images/home_hover.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = 'white';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(HomeTab);

	// Create an ImageView.
	var homeicon = Ti.UI.createImageView({
		image : '/images/home.png',
		width : '40%',
		height : '50%',
		top : '13%'
	});

	// Add to the parent view.
	HomeTab.add(homeicon);

	// Create a Label.
	var Homelabel = Ti.UI.createLabel({
		text : 'HOME',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '0%',
		textAlign : 'center'
	});

	// Add to the parent view.
	HomeTab.add(Homelabel);

	var AccountTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	AccountTab.addEventListener('click', function() {
		var Account = require('/ui/common/Sales');
		var account = new Account();
		Allview.add(account);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user_hover.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = 'white';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(AccountTab);

	// Create an ImageView.
	var Accounticon = Ti.UI.createImageView({
		image : '/images/user.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	AccountTab.add(Accounticon);

	// Create a Label.
	var Accountlabel = Ti.UI.createLabel({
		text : 'ACCOUNT',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	AccountTab.add(Accountlabel);

	var ShopTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	ShopTab.addEventListener('click', function() {
		var Shop = require('/ui/common/Shop');
		var shop = new Shop();
		Allview.add(shop);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye_hover.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = 'white';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';
	});
	bottomview.add(ShopTab);

	// Create an ImageView.
	var Shopicon = Ti.UI.createImageView({
		image : '/images/eye.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	ShopTab.add(Shopicon);

	// Create a Label.
	var Shoplabel = Ti.UI.createLabel({
		text : 'SHOP',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	ShopTab.add(Shoplabel);

	var CartTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	CartTab.addEventListener('click', function() {
		var Cart = require('/ui/common/Cart');
		var cart = new Cart();
		Allview.add(cart);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart_hover.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = 'white';
		Morelabel.color = '#838383';
	});
	bottomview.add(CartTab);

	// Create an ImageView.
	var Carticon = Ti.UI.createImageView({
		image : '/images/cart.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	CartTab.add(Carticon);

	// Create a Label.
	var Cartlabel = Ti.UI.createLabel({
		text : 'CART',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	CartTab.add(Cartlabel);

	var MoreTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	MoreTab.addEventListener('click', function() {
		var More = require('/ui/common/More');
		var more = new More();
		Allview.add(more);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more_hover.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = 'white';
	});
	bottomview.add(MoreTab);

	// Create an ImageView.
	var Moreicon = Ti.UI.createImageView({
		image : '/images/more_hover.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	MoreTab.add(Moreicon);

	// Create a Label.
	var Morelabel = Ti.UI.createLabel({
		text : 'MORE',
		color : 'white',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	MoreTab.add(Morelabel);

	//********************************************************************End Bottom Tab*************************************************************


	return Allview;
}

module.exports = More;
