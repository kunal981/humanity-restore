function SubCategory(_sub_category_name_receive,_sub_category_id_receive) {
	

	var font1 = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
	var font2 = (Titanium.Platform.displayCaps.platformHeight * 2.5) / 100;
	var font3 = (Titanium.Platform.displayCaps.platformHeight * 2.2) / 100;
	var font4 = (Titanium.Platform.displayCaps.platformHeight * 2) / 100;
	var font5 = (Titanium.Platform.displayCaps.platformHeight * 1.5) / 100;

	var border = (Titanium.Platform.displayCaps.platformHeight * 0.5) / 100;
	var border2 = (Titanium.Platform.displayCaps.platformHeight * 0.8) / 100;

	var hgt = Ti.Platform.displayCaps.platformHeight;
	var wdh = Ti.Platform.displayCaps.platformWidth;
	
	

	var Allview = Ti.UI.createView({
		top : 0,
		//backgroundColor : '#E73A2F',
		height : '100%',
		width : '100%'
	});

	var topView = Ti.UI.createView({
		backgroundImage : '/images/header_bg.png',
		backgroundColor : '#5F6465',
		height : '10%',
		width : '100%',
		top : 0
	});

	Allview.add(topView);

	// Create a Button.
	var shopbutton = Ti.UI.createButton({
		backgroundImage : '/images/back.png',
		title : ' Shop',
		color : 'white',
		font : {
			fontSize : 14
		},
		style : 'none',
		height : '55%',
		width : '17%',
		left : '0.8%',
		borderRadius : border
	});

	shopbutton.addEventListener('touchstart', function(e) {

		shopbutton.backgroundImage = '/images/back_hover.png';

	});
	shopbutton.addEventListener('touchcancel', function(e) {

		shopbutton.backgroundImage = '/images/back.png';

	});
	shopbutton.addEventListener('touchend', function(e) {

		shopbutton.backgroundImage = '/images/back.png';

	});

	// Listen for click events.
	shopbutton.addEventListener('click', function() {
		
		
		Ti.UI.currentWindow.remove(Allview);
	});

	// Add to the parent view.
	topView.add(shopbutton);

	// Create a Label.
	var shoplabel = Ti.UI.createLabel({
		text : 'Men',
		color : 'white',
		font : {
			fontSize : font1
		},
		textAlign : 'center'
	});

	// Add to the parent view.
	topView.add(shoplabel);

	var body = Ti.UI.createView({
		backgroundImage : '/images/magento.png',
		width : '100%',
		height : '90%',
		top : '10%'
	});
	Allview.add(body);

	// to fit in a 320-wide space

	var tableData = [];

	var count = 1;

	for (var y = 0; y < _sub_category_id_receive.length; y++) {
		var thisRow = Ti.UI.createTableViewRow({
			height : hgt * 0.1,
			backgroundColor : 'transparent',
			selectedColor:'#E89F4F',
			backgroundFocusedColor : '#E89F4F',
			focusable : true,
			backgroundSelectedColor : '#E89F4F',
			id:y
		});
		thisRow.addEventListener('click', function(e) {
			var tmp=e.source.id;
			
			Ti.App.Properties.setString('cat_id_', _sub_category_id_receive[tmp]);
			
			var ProductListing = require('/ui/common/ProductListing');
			var productListing = new ProductListing();
			Ti.UI.currentWindow.add(productListing);
		});

		// Create a Label.
		var categoryText = Ti.UI.createLabel({
			text : _sub_category_name_receive[count] ,
			color : 'white',
			font : {
				fontSize : font1
			},
			left : wdh * 0.05,
			textAlign : 'center',
			id:y
		});

		// Add to the parent view.
		thisRow.add(categoryText);

		// Create an ImageView.
		var arrow = Ti.UI.createImageView({
			image : '/images/arrow.png',
			width : wdh * 0.1,
			height : hgt * 0.05,
			right : wdh * 0.05,
			id:y
		});
		arrow.addEventListener('load', function() {
			Ti.API.info('Image loaded!');
		});

		// Add to the parent view.
		thisRow.add(arrow);

		tableData.push(thisRow);

		count++;
	}

	var tableview = Ti.UI.createTableView({
		data : tableData,
		backgroundColor : 'transparent',
	});

	tableview.addEventListener("click", function(e) {
		if (e.source.objName) {
			Ti.API.info("---> " + e.source.objName + e.source.objIndex + " Its click");
		}
	});

	body.add(tableview);
	//*************************************************************************Bottom Tabs************************************************************************

	var bottomview = Ti.UI.createView({
		backgroundImage : '/images/footer_bg.png',
		backgroundColor : '#363D40',
		bottom : 0,
		height : '10%',
		width : '100%',
		layout : 'horizontal'
	});
	Allview.add(bottomview);

	var HomeTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	HomeTab.addEventListener('click', function() {
		var Home = require('/ui/common/Home');
		var home = new Home();
		Allview.add(home);
		homeicon.image = '/images/home_hover.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = 'white';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(HomeTab);

	// Create an ImageView.
	var homeicon = Ti.UI.createImageView({
		image : '/images/home.png',
		width : '40%',
		height : '50%',
		top : '13%'
	});

	// Add to the parent view.
	HomeTab.add(homeicon);

	// Create a Label.
	var Homelabel = Ti.UI.createLabel({
		text : 'HOME',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '0%',
		textAlign : 'center'
	});

	// Add to the parent view.
	HomeTab.add(Homelabel);

	var AccountTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	AccountTab.addEventListener('click', function() {
		var Account = require('/ui/common/Sales');
		var account = new Account();
		Allview.add(account);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user_hover.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = 'white';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(AccountTab);

	// Create an ImageView.
	var Accounticon = Ti.UI.createImageView({
		image : '/images/user.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	AccountTab.add(Accounticon);

	// Create a Label.
	var Accountlabel = Ti.UI.createLabel({
		text : 'ACCOUNT',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	AccountTab.add(Accountlabel);

	var ShopTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	ShopTab.addEventListener('click', function() {
		var Shop = require('/ui/common/Shop');
		var shop = new Shop();
		Allview.add(shop);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye_hover.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = 'white';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';
	});
	bottomview.add(ShopTab);

	// Create an ImageView.
	var Shopicon = Ti.UI.createImageView({
		image : '/images/eye_hover.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	ShopTab.add(Shopicon);

	// Create a Label.
	var Shoplabel = Ti.UI.createLabel({
		text : 'SHOP',
		color : 'white',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	ShopTab.add(Shoplabel);

	var CartTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	CartTab.addEventListener('click', function() {
		var Cart = require('/ui/common/Cart');
		var cart = new Cart();
		Allview.add(cart);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart_hover.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = 'white';
		Morelabel.color = '#838383';
	});
	bottomview.add(CartTab);

	// Create an ImageView.
	var Carticon = Ti.UI.createImageView({
		image : '/images/cart.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	CartTab.add(Carticon);

	// Create a Label.
	var Cartlabel = Ti.UI.createLabel({
		text : 'CART',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	CartTab.add(Cartlabel);

	var MoreTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	MoreTab.addEventListener('click', function() {
		var More = require('/ui/common/More');
		var more = new More();
		Allview.add(more);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more_hover.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = 'white';
	});
	bottomview.add(MoreTab);

	// Create an ImageView.
	var Moreicon = Ti.UI.createImageView({
		image : '/images/more.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	MoreTab.add(Moreicon);

	// Create a Label.
	var Morelabel = Ti.UI.createLabel({
		text : 'MORE',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	MoreTab.add(Morelabel);

	return Allview;
}

module.exports = SubCategory;
