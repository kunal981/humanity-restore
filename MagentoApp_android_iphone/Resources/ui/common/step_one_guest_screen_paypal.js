var guest_first_screen = Ti.UI.currentWindow;

var view = Ti.UI.createView({
	width : '100%',
	height : '100%',
});

guest_first_screen.add(view);

var topView = Ti.UI.createView({
	backgroundImage : '/images/header_bg.png',
	backgroundColor : '#5F6465',
	height : '10%',
	width : '100%',
	top : 0
});

view.add(topView);

// Create a Button.
var aButton = Ti.UI.createButton({
	title : ' Back',
	backgroundImage : '/images/back.png',
	font : {
		fontSize : 13,
		fontWeight : 'bold',
	},
	color : 'white',

	height : '55%',
	width : '17%',
	left : '0.8%',
});

aButton.addEventListener('touchstart', function(e) {

		aButton.backgroundImage = '/images/back_hover.png';

	});
	aButton.addEventListener('touchcancel', function(e) {

		aButton.backgroundImage = '/images/back.png';

	});
	aButton.addEventListener('touchend', function(e) {

		aButton.backgroundImage = '/images/back.png';

	});
aButton.addEventListener('click', function() {
	Ti.App.Properties.setString('email_guest', null);
			Ti.App.Properties.setString('firstname_guest', null);
			Ti.App.Properties.setString('lastname_guest', null);
	guest_first_screen.close();
	/*var app =Ti.UI.createWindow({
	backgroundColor:'white',
	url:'ui/common/Checkout_Paypal.js',
	navBarHidden:true,
	fullscreen:true,
	
});
app.open();

*/
});
topView.add(aButton);

// Create a Label.
var aLabel = Ti.UI.createLabel({
	text : 'STEP 1:Personal Information',
	color : 'white',
	font : {
		fontSize : 15
	},
	height : 'auto',
	width : 'auto',

	textAlign : 'center'
});

// Add to the parent view.
topView.add(aLabel);

var view_scroll = Ti.UI.createScrollView({
	width : '100%',
	height : '90%',
	top:'10%'
});

view.add(view_scroll);

// Create a TextField.
var aTextField_email = Ti.UI.createTextField({
	height : '8%',
	top : '5%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'email',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_email);



// Create a TextField.
var aTextField_firstname = Ti.UI.createTextField({
	height : '8%',
	top : '19%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'firstname',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_firstname);

// Create a TextField.
var aTextField_lastname = Ti.UI.createTextField({
	height : '8%',
	top : '33%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'lastname',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_lastname);

var aButton_register = Ti.UI.createButton({
	backgroundImage : '/images/backimage_button.png',
	style : 'none',
	font : {
		fontSize : 14,
		fontWeight : 'bold',
	},
	color : 'white',
	title : 'Continue',
	height : '10%',

	top : '50%',

	width : '40%',
	right : '10%',

});
aButton_register.addEventListener('touchstart', function(e) {

		aButton_register.backgroundImage = '/images/backimage_hover_button.png';

	});
	aButton_register.addEventListener('touchcancel', function(e) {

		aButton_register.backgroundImage = '/images/backimage_button.png';

	});
	aButton_register.addEventListener('touchend', function(e) {

		aButton_register.backgroundImage = '/images/backimage_button.png';

	});

//   register work*******************************start work************++++++++++++++++++++++++++++++++++++++++++++++++++=============
// Listen for click events.
aButton_register.addEventListener('click', function() {
	
	if(aTextField_email.value.length==''){
		alert('please fill email');
	}
	else if(aTextField_firstname.value.length==''){
		alert('please fill firstname');
	}
	else if(aTextField_lastname.value.length==''){
		alert('please fill lastname');
	}
	else if(aTextField_email.value.length=='' && aTextField_firstname.value.length=='' && aTextField_lastname.value.length==''){
		alert('please fill');
	}
	else{
		    Ti.App.Properties.setString('email_guest', aTextField_email.value);
			Ti.App.Properties.setString('firstname_guest', aTextField_firstname.value);
			Ti.App.Properties.setString('lastname_guest', aTextField_lastname.value);
			
		var app = Ti.UI.createWindow({
		backgroundColor : 'white',
		url : '/ui/common/step_two_guest_billing.js',
		navBarHidden : true,
		fullscreen : true,
		modal:true
		
	});
	app.open();
	}

	

});
aButton_register.addEventListener('touchstart', function(e) {

	aButton_register.backgroundImage = '/images/backimage_hover_button.png';

});
aButton_register.addEventListener('touchcancel', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});
aButton_register.addEventListener('touchend', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});

// Add to the parent view.
view_scroll.add(aButton_register); 