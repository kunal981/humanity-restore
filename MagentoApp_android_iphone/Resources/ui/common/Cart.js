function Cart() {

	var font1 = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
	var font2 = (Titanium.Platform.displayCaps.platformHeight * 2.2) / 100;
	var font3 = (Titanium.Platform.displayCaps.platformHeight * 2) / 100;
	var font4 = (Titanium.Platform.displayCaps.platformHeight * 2.5) / 100;
	var font5 = (Titanium.Platform.displayCaps.platformHeight * 1.5) / 100;

	var border = (Titanium.Platform.displayCaps.platformHeight * 0.5) / 100;
	
	var myDatabase;
	var _product_id = [];
	var name_product = [];
	var _quatity_ = [];
	var _price_ = [];
	var image_path = [];
	var product_string = '';
	var qualitity_string = '';
	var product_string = '';
	var qualitity_string;
	var rows;
	
	myDatabase = Ti.Database.open('ProductInformationSave');
	var sql = 'CREATE TABLE IF NOT EXISTS product_details(' + '_product_id INTEGER PRIMARY KEY , _quatity INTEGER, _single_price  INTEGER ,_image_path,name_product' + ')';
	myDatabase.execute(sql);
	myDatabase.close();
	
	//var _product_id=Ti.App.Properties.getString('_id_product');
	
	//myDatabase = Ti.Database.install('/ProductInformationSave.sqlite', 'ProductInformationSave');
	myDatabase = Ti.Database.open('ProductInformationSave');
	var get_info_database = myDatabase.execute('SELECT * from product_details');
	rows = get_info_database.getRowCount();
	Ti.API.error('rows===' + rows);
	while (get_info_database.isValidRow()) {
		Ti.API.error('enter while condition');
		_product_id.push(get_info_database.fieldByName('_product_id'));
		_quatity_.push(get_info_database.fieldByName('_quatity'));
		_price_.push(get_info_database.fieldByName('_single_price'));
		image_path.push(get_info_database.fieldByName('_image_path'));
		name_product.push(get_info_database.fieldByName('name_product'));

		get_info_database.next();
	}

	get_info_database.close();
	myDatabase.close();
	Ti.API.error('_quatity_=====' + _quatity_);
	Ti.API.error('_price_=====' + _price_);
	Ti.API.error('image_path=====' + image_path);
	Ti.API.error('name_product=====' + name_product);

	var total_sum = 0;
	for (var i = 0; i < _product_id.length; i++) {
		Ti.API.error("_quatity_[i]  " + _quatity_[i] + " _price_[i] " + _price_[i] + " i " + i);
		total_sum = total_sum + (_quatity_[i] * _price_[i]);

	}
	var final_sum = total_sum.toFixed(2);

	for (var j = 0; j < _product_id.length; j++) {
		if (j == 0) {
			product_string = _product_id[0];
		} else {
			product_string = product_string + "," + _product_id[j];
		}
		Ti.API.error("product_string[i]  " + product_string + " _product_id " + _product_id[j] + " j " + j);

	}

	for (var k = 0; k < _quatity_.length; k++) {

		if (k == 0) {

			qualitity_string = _quatity_[0];
		} else {
			qualitity_string = qualitity_string + "," + _quatity_[k];
		}

	}
	Ti.API.error("total_sum====" + total_sum);
	Ti.API.error("qualitity_string===" + qualitity_string);
	Ti.API.error("product_string===" + product_string);
	Ti.API.error("_quatity_" + _quatity_);
	Ti.API.error("_price_" + _price_);


	
	var selected_item = Ti.App.Properties.getString('selected_item');

	var hgt = Titanium.Platform.displayCaps.platformHeight;
	var wdh = Ti.Platform.displayCaps.platformWidth;

	var Allview = Ti.UI.createView({
		backgroundColor : '#F3F3F3',
		top : 0,
		height : '100%',
		width : '100%',
		animated : false,
		transition : Titanium.UI.iPhone.AnimationStyle.NONE,
	});
	
	

	var indicatorView = Ti.UI.createView({
		backgroundColor : 'black',
		height : '25%',
		width : '40%',
		opacity : 0.7,
		borderRadius : 10
	});
// change android/iphone
	var activityIndicator = Ti.UI.createActivityIndicator({
		style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,

	});
	indicatorView.add(activityIndicator);

	var topView = Ti.UI.createView({
		backgroundImage : '/images/header_bg.png',
		backgroundColor : '#5F6465',
		height : '10%',
		width : '100%',
		top : 0
	});

	Allview.add(topView);

	// Create a Button.
	var shopbutton = Ti.UI.createButton({
		backgroundImage : '/images/login_another.png',
		style : 'none',
		font : {
			fontSize : 15,
			fontWeight : 'bold',
		},
		title : 'Edit',
		color : 'white',
		height : '55%',
		width : '20%',
		left : '2%',
		borderRadius : border
	});

	shopbutton.addEventListener('touchstart', function(e) {

		shopbutton.backgroundImage = '/images/login_hover.png';

	});
	shopbutton.addEventListener('touchcancel', function(e) {

		shopbutton.backgroundImage = '/images/login_another.png';

	});
	shopbutton.addEventListener('touchend', function(e) {

		shopbutton.backgroundImage = '/images/login_another.png';

	});

	// Listen for click events.
	shopbutton.addEventListener('click', function() {
		var Shop = require('/ui/common/EditSection');
		var shop = new Shop();
		Allview.add(shop);
	});

	// Add to the parent view.
	topView.add(shopbutton);

	// Create a Button.
	var cartbutton = Ti.UI.createButton({
		title : 'CheckOut',
		backgroundImage : '/images/backimage_button.png',
		font : {
			fontSize : 15,
			fontWeight : 'bold',
		},
		color : 'white',

		height : '55%',
		width : '30%',
		right : '2%',
		borderRadius : border
	});
	cartbutton.addEventListener('touchstart', function(e) {

		cartbutton.backgroundImage = '/images/backimage_hover_button.png';

	});
	cartbutton.addEventListener('touchcancel', function(e) {

		cartbutton.backgroundImage = '/images/backimage_button.png';

	});
	cartbutton.addEventListener('touchend', function(e) {

		cartbutton.backgroundImage = '/images/backimage_button.png';

	});

	cartbutton.addEventListener('click', function() {

		Allview.add(AllCart);
	});

	// Add to the parent view.
	topView.add(cartbutton);

	// Create an ImageView.
	var iconTop = Ti.UI.createImageView({
		image : '/images/icon.png',
		width : '15%',
		height : '80%',
		left : '42%'
	});
	iconTop.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});

	// Add to the parent view.
	topView.add(iconTop);

	/*
	 // Create a Label.
	 var shoplabel = Ti.UI.createLabel({
	 text : 'Cart',
	 color : 'white',
	 font : {
	 fontSize : font1
	 },
	 left : '48%',
	 textAlign : 'center'
	 });

	 // Add to the parent view.
	 topView.add(shoplabel);*/

	var body = Ti.UI.createScrollView({
		backgroundColor : '#F3F3F3',
		top : '10%',
		height : '80%',
		width : '100%',
		layout : 'vertical'
	});
	Allview.add(body);

	var productTitleView = Ti.UI.createView({
		backgroundImage : '/images/header_bg.png',
		backgroundColor : '#5F6465',
		height : hgt * 0.07,
		width : '100%',
		top : '0.5%'
	});

	body.add(productTitleView);

	// Create a Label.
	var staticProductlabel = Ti.UI.createLabel({
		text : 'Product',
		color : 'white',
		font : {
			fontSize : font1
		},
		left : '4%',
		textAlign : 'center'
	});

	// Add to the parent view.
	productTitleView.add(staticProductlabel);

	// Create a Label.

	var staticQtylabel = Ti.UI.createLabel({
		text : 'Qty',
		color : 'white',
		font : {
			fontSize : font1
		},
		right : '4%',
		textAlign : 'center'
	});

	// Add to the parent view.
	productTitleView.add(staticQtylabel);

	var GetProductView = Ti.UI.createView({
		backgroundColor : '#5F6465',
		height : Ti.UI.SIZE,
		width : '100%',
		top : '0.5%'
	});

	body.add(GetProductView);
	if (rows == 0) {

		// Create a Label.
		var aLabel = Ti.UI.createLabel({
			text : 'No Product Add',
			color : '#ffffff',
			font : {
				fontSize : 15
			},
			height : 'auto',
			width : 'auto',

			textAlign : 'center'
		});

		// Add to the parent view.
		GetProductView.add(aLabel);

	}

	fetch_data();

	function fetch_data() {

		// create  view==========================================*************************************************************
		var colorSetIndex = 0;
		var cellIndex = 0;
		var tableData = [];
		var repeatGetProductView = [];

		var thisRow = [];

		for (var j = 0; j < name_product.length; j++) {

			thisRow[j] = Ti.UI.createView({
				layout : "vertical",
				height : hgt * 0.2,

				width : '100%',

				selectedColor : '#E89F4F',
				backgroundColor : '#D3D3D3',
				backgroundFocusedColor : '#E89F4F',
				focusable : true,
				backgroundSelectedColor : '#E89F4F',

			});
			body.add(thisRow[j]);

			var line_view = Ti.UI.createView({
				width : '100%',
				height : 1,

				backgroundColor : '#000000'
			});
			thisRow[j].add(line_view);

			for (var i = 0; i < name_product.length; i++) {

				repeatGetProductView[colorSetIndex] = Ti.UI.createView({
					height : hgt * 0.2,
					width : '100%',
					layout : 'horizontal'
				});
				thisRow[j].add(repeatGetProductView[colorSetIndex]);

				// Create an ImageView.
				var productImage = Ti.UI.createImageView({
					image : image_path[j],
					width : wdh * 0.3,
					height : hgt * 0.15,
					top : hgt * 0.025,
					left : wdh * 0.02,
				});
				productImage.addEventListener('load', function() {
					Ti.API.info('Image loaded!');
				});

				// Add to the parent view.
				repeatGetProductView[colorSetIndex].add(productImage);

				var detailView = Ti.UI.createView({
					height : hgt * 0.15,
					width : wdh * 0.45,
					layout : 'vertical',
					left : '5%',
					top : hgt * 0.025,
				});

				repeatGetProductView[colorSetIndex].add(detailView);

				// Create a Label.
				var productNamelabel = Ti.UI.createLabel({
					text : name_product[j],
					color : 'black',
					font : {
						fontSize : font2
					},
					left : '0%',

				});

				// Add to the parent view.
				detailView.add(productNamelabel);

				// Create a Label.
				var unitpricelabel = Ti.UI.createLabel({
					text : 'unitprice: ' + _price_[j],
					color : 'black',
					font : {
						fontSize : font3
					},
					left : '0%',
				});

				// Add to the parent view.
				detailView.add(unitpricelabel);

				// Create a Label.
				/*
				var subtotallabel = Ti.UI.createLabel({
				text : 'subtotal: ',
				color : 'black',
				font : {
				fontSize : font3
				},
				left : '0%',
				});

				// Add to the parent view.
				detailView.add(subtotallabel);*/

				// Create a Label.
				var totalQtylabel = Ti.UI.createLabel({
					text : _quatity_[j],
					color : 'black',
					font : {
						fontSize : font3
					},
					left : '10%',
				});

				// Add to the parent view.
				repeatGetProductView[colorSetIndex].add(totalQtylabel);
				cellIndex++;
				colorSetIndex++;

			}

		}

		//*******************************************************************end*************************************************************
	}

	var GetProducttitleView = Ti.UI.createView({
		backgroundColor : '#D1D1D1',
		height : hgt * 0.08,
		width : '100%',
		top : '0%'
	});

	body.add(GetProducttitleView);

	// Create a TextField.
	var aTextField = Ti.UI.createTextField({
		top : '10%',
		height : '90%',
		width : '90%',
		borderRadius : border,
		hintText : 'Enter discount code if any',
		font : {
			fontSize : font2
		},
		keyboardType : Ti.UI.KEYBOARD_DEFAULT,
		returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
		borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
	});

	// Listen for return events.
	aTextField.addEventListener('return', function(e) {
		aTextField.blur();
		alert('Input was: ' + aTextField.value);
	});

	// Add to the parent view.
	GetProducttitleView.add(aTextField);

	var priceView = Ti.UI.createView({
		height : hgt * 0.15,
		width : '100%',
		layout : 'horizontal'
	});

	body.add(priceView);

	// Create an ImageView.
	var aboutimages = Ti.UI.createView({
		backgroundImage : '/images/about-1.png',
		width : '10%',
		height : '40%',
		left : '5%'
	});
	aboutimages.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});

	// Add to the parent view.
	priceView.add(aboutimages);

	var priceViewstatictitle = Ti.UI.createView({
		height : hgt * 0.13,
		layout : 'vertical',
		width : '25%',
		left : '18%',
		top : '15%'
	});

	priceView.add(priceViewstatictitle);

	// Create a Label.
	var subpriceViewstatictitle1 = Ti.UI.createLabel({
		text : 'subtotal: ',
		color : 'black',
		font : {
			fontSize : font4
		},
		right : '0%',
	});

	// Add to the parent view.
	priceViewstatictitle.add(subpriceViewstatictitle1);

	// Create a Label.
	/*
	var subpriceViewstatictitle2 = Ti.UI.createLabel({
	text : 'Tax',
	color : 'black',
	font : {
	fontSize : font4
	},
	right : '0%',
	});

	// Add to the parent view.
	priceViewstatictitle.add(subpriceViewstatictitle2);*/

	// Create a Label.
	var subpriceViewstatictitle3 = Ti.UI.createLabel({
		text : 'Grand Total:',
		color : 'black',
		font : {
			fontSize : font4,
			fontWeight : 'bold',
		},
		right : '0%',
	});

	// Add to the parent view.
	priceViewstatictitle.add(subpriceViewstatictitle3);

	var priceViewvarytitle = Ti.UI.createView({
		height : hgt * 0.13,
		layout : 'vertical',
		width : '40.5%',
		top : '15%',

	});

	priceView.add(priceViewvarytitle);

	// Create a Label.
	var subpriceViewstatictitle1 = Ti.UI.createLabel({
		text : '$' + final_sum,
		color : '#E75E01',
		font : {
			fontSize : font4
		},
		right : '2%',
	});

	// Add to the parent view.
	priceViewvarytitle.add(subpriceViewstatictitle1);

	// Create a Label.
	var subpriceViewstatictitle3 = Ti.UI.createLabel({
		text : '$' + final_sum,
		color : '#E75E01',
		font : {
			fontSize : font4
		},
		right : '2%',
	});

	// Add to the parent view.
	priceViewvarytitle.add(subpriceViewstatictitle3);

	var likeTitleView = Ti.UI.createView({
		backgroundImage : '/images/header_bg.png',
		backgroundColor : '#5F6465',
		height : hgt * 0.07,
		width : '100%',
	});

	body.add(likeTitleView);

	/*
	// Create a Label.
	var likeTitle = Ti.UI.createLabel({
	text : 'You may also like',
	color : 'white',
	font : {
	fontSize : font4,
	fontWeight : 'bold',
	},
	left : '7%',
	});
	*/

	// Add to the parent view.
	//likeTitleView.add(likeTitle);
	// create ***********************************************table view----------------------------------------**********************************************************

	/*
	var count = 1;
	var colorSetIndex = 0;
	var cellIndex = 0;
	var tableData = [];

	var LikeProductView = [];
	var thisRow = [];

	for (var i = 0; i < _product_id.length; i++) {
	thisRow[i] = Ti.UI.createTableViewRow({
	layout : "vertical",
	height : hgt * 0.2,
	//backgroundColor : 'transparent',
	selectedColor : '#E89F4F',
	backgroundColor : '#D3D3D3',
	backgroundFocusedColor : '#E89F4F',
	focusable : true,
	backgroundSelectedColor : '#E89F4F',

	});

	thisRow[i].addEventListener('touchstart', function(e) {
	//	e.source.setBackgroundColor('red');
	//alert(colorSetIndex);
	thisRow[e.index].setBackgroundColor('#E89F4F');
	// just for effect
	});
	thisRow[i].addEventListener('touchcancel', function(e) {
	//	e.source.setBackgroundColor('white');
	thisRow[e.index].setBackgroundColor('#D3D3D3');
	// just for effect
	});
	thisRow[i].addEventListener('touchend', function(e) {
	//	e.source.setBackgroundColor('white');
	thisRow[e.index].setBackgroundColor('#D3D3D3');
	// just for effect
	});

	thisRow[i].addEventListener('click', function(e) {

	var tmp = e.index;
	//alert(tmp);
	/*
	Ti.App.Properties.setString('selected_item', 'Nokia Phone 1');
	var ProductInformationPage = require('ProductInformationPage');
	var productInformationPage = new ProductInformationPage();
	Ti.UI.currentWindow.add(productInformationPage);*/

	///	});

	//alert(colorSetIndex);
	/*
	LikeProductView[colorSetIndex] = Ti.UI.createView({
	height : hgt * 0.2,
	width : '100%',
	id : colorSetIndex
	});*/

	/*
	LikeProductView[colorSetIndex].addEventListener('click', function(e) {
	var tmp = e.source.id;
	//alert(tmp);
	Ti.App.Properties.setString('selected_item', 'Nokia Phone 1');
	var ProductInformationPage = require('ProductInformationPage');
	var productInformationPage = new ProductInformationPage();
	Ti.UI.currentWindow.add(productInformationPage);
	});
	LikeProductView[colorSetIndex].addEventListener('touchstart', function(e) {
	//	e.source.setBackgroundColor('#c60000');
	//alert(colorSetIndex);
	LikeProductView[e.source.id].setBackgroundColor('#c60000');
	// just for effect
	});
	LikeProductView[colorSetIndex].addEventListener('touchcancel', function(e) {
	//	e.source.setBackgroundColor('white');
	LikeProductView[e.source.id].setBackgroundColor('#F3F3F3');
	// just for effect
	});
	LikeProductView[colorSetIndex].addEventListener('touchend', function(e) {
	//	e.source.setBackgroundColor('white');
	LikeProductView[e.source.id].setBackgroundColor('#F3F3F3');
	// just for effect
	});*/

	/*	thisRow[i].add(LikeProductView[colorSetIndex]);

	// Create an ImageView.
	var productImage = Ti.UI.createImageView({
	image : image_path[colorSetIndex],
	width : wdh * 0.3,
	height : hgt * 0.15,
	left : wdh * 0.02,
	id : colorSetIndex,
	borderColor : 'white',
	borderWidth : 3
	});
	productImage.addEventListener('load', function() {

	});

	// Add to the parent view.
	LikeProductView[colorSetIndex].add(productImage);

	var detail = Ti.UI.createView({
	width : wdh * 0.5,
	height : hgt * 0.15,
	left : wdh * 0.35,
	layout : 'vertical',
	id : colorSetIndex
	});
	LikeProductView[colorSetIndex].add(detail);

	// Create a Label.
	var categoryText1 = Ti.UI.createLabel({
	text : 'Nokia Phone ' + count,
	color : 'black',
	font : {
	fontSize : font2,
	fontWeight : 'bold',
	},
	left : 0,
	id : colorSetIndex
	});

	// Add to the parent view.
	detail.add(categoryText1);

	// Create a Label.
	var categoryText2 = Ti.UI.createLabel({
	text : 'Regular $'+_price_[colorSetIndex],
	color : 'black',
	font : {
	fontSize : font3
	},
	left : 0,
	id : colorSetIndex
	});

	// Add to the parent view.
	detail.add(categoryText2);

	// Create a Label.
	var categoryText3 = Ti.UI.createLabel({
	text : 'Special $546565',
	color : 'black',
	font : {
	fontSize : font3
	},
	left : 0,
	id : colorSetIndex
	});

	// Add to the parent view.
	detail.add(categoryText3);

	// Create a Label.
	var categoryText4 = Ti.UI.createLabel({
	text : 'Quatity: '+_quatity_[colorSetIndex],
	color : 'black',
	font : {
	fontSize : font3
	},
	left : 0,
	id : colorSetIndex
	});

	// Add to the parent view.
	detail.add(categoryText4);

	// Create an ImageView.
	var arrow = Ti.UI.createImageView({
	image : '/images/arrow.png',
	width : wdh * 0.1,
	height : hgt * 0.05,
	right : wdh * 0.02,
	top : hgt * 0.07,
	id : colorSetIndex
	});
	arrow.addEventListener('load', function() {
	Ti.API.info('Image loaded!');
	});

	// Add to the parent view.
	LikeProductView[colorSetIndex].add(arrow);
	cellIndex++;
	colorSetIndex++;

	tableData.push(thisRow[i]);
	}

	var tableview = Ti.UI.createTableView({
	data : tableData,
	backgroundColor : 'transparent',
	//scrollable : false
	});

	tableview.addEventListener("click", function(e) {
	if (e.source.objName) {
	Ti.API.info("---> " + e.source.objName + e.source.objIndex + " was clicked!");
	}
	});
	body.add(tableview);*/

	//*************************************************************************end table view************************************************************************

	//*************************************************************************Bottom Tabs************************************************************************

	var bottomview = Ti.UI.createView({
		backgroundImage : '/images/footer_bg.png',
		backgroundColor : '#363D40',
		bottom : 0,
		height : '10%',
		width : '100%',
		layout : 'horizontal'
	});
	Allview.add(bottomview);

	var HomeTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	HomeTab.addEventListener('click', function() {
		var Home = require('/ui/common/Home');
		var home = new Home();
		Allview.add(home);
		homeicon.image = '/images/home_hover.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = 'white';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(HomeTab);

	// Create an ImageView.
	var homeicon = Ti.UI.createImageView({
		image : '/images/home.png',
		width : '40%',
		height : '50%',
		top : '13%'
	});

	// Add to the parent view.
	HomeTab.add(homeicon);

	// Create a Label.
	var Homelabel = Ti.UI.createLabel({
		text : 'HOME',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '0%',
		textAlign : 'center'
	});

	// Add to the parent view.
	HomeTab.add(Homelabel);

	var AccountTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	AccountTab.addEventListener('click', function() {
		var Account = require('/ui/common/Sales');
		var account = new Account();
		Allview.add(account);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user_hover.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = 'white';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(AccountTab);

	// Create an ImageView.
	var Accounticon = Ti.UI.createImageView({
		image : '/images/user.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	AccountTab.add(Accounticon);

	// Create a Label.
	var Accountlabel = Ti.UI.createLabel({
		text : 'ACCOUNT',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	AccountTab.add(Accountlabel);

	var ShopTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	ShopTab.addEventListener('click', function() {
		var Shop = require('/ui/common/Shop');
		var shop = new Shop();
		Allview.add(shop);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye_hover.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = 'white';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';
	});
	bottomview.add(ShopTab);

	// Create an ImageView.
	var Shopicon = Ti.UI.createImageView({
		image : '/images/eye.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	ShopTab.add(Shopicon);

	// Create a Label.
	var Shoplabel = Ti.UI.createLabel({
		text : 'SHOP',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	ShopTab.add(Shoplabel);

	var CartTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	CartTab.addEventListener('click', function() {
		var Cart = require('/ui/common/Cart');
		var cart = new Cart();
		Allview.add(cart);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart_hover.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = 'white';
		Morelabel.color = '#838383';
	});
	bottomview.add(CartTab);

	// Create an ImageView.
	var Carticon = Ti.UI.createImageView({
		image : '/images/cart_hover.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	CartTab.add(Carticon);

	// Create a Label.
	var Cartlabel = Ti.UI.createLabel({
		text : 'CART',
		color : 'white',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	CartTab.add(Cartlabel);

	var MoreTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	MoreTab.addEventListener('click', function() {
		var More = require('/ui/common/More');
		var more = new More();
		Allview.add(more);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more_hover.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = 'white';
	});
	bottomview.add(MoreTab);

	// Create an ImageView.
	var Moreicon = Ti.UI.createImageView({
		image : '/images/more.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	MoreTab.add(Moreicon);

	// Create a Label.
	var Morelabel = Ti.UI.createLabel({
		text : 'MORE',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	MoreTab.add(Morelabel);

	//********************************************************************End Bottom Tab*************************************************************

	//********************************************************************check out*************************************************************

	var AllCart = Ti.UI.createView({
		height : '100%',
		width : '100%',
	});

	var AllCartview = Ti.UI.createView({
		backgroundColor : 'black',
		height : '60%',
		width : '100%',
		top : '0%',
		opacity : 0.8
	});
	AllCartview.addEventListener('click', function() {
		Allview.remove(AllCart);
	});

	AllCart.add(AllCartview);

	var AllCartBottomview = Ti.UI.createView({
		backgroundColor : 'white',
		height : '40%',
		width : '100%',
		top : '60%',
	});

	AllCart.add(AllCartBottomview);

	var topCartView = Ti.UI.createView({
		backgroundImage : '/images/header_bg.png',
		backgroundColor : '#5F6465',
		height : '20%',
		width : '100%',
		top : 0
	});

	AllCartBottomview.add(topCartView);

	// Create a Label.
	var shopCartlabel = Ti.UI.createLabel({
		text : 'Checkout',
		color : 'white',
		font : {
			fontSize : font1
		},

		textAlign : 'center'
	});

	// Add to the parent view.
	topCartView.add(shopCartlabel);

	var bottomCartProductView = Ti.UI.createView({
		backgroundColor : '#D3D3D3',
		height : '80%',
		width : '100%',
		top : '20%'
	});
	AllCartBottomview.add(bottomCartProductView);

	// Create a Button.
	var paypalbutton = Ti.UI.createButton({
		backgroundImage : '/images/paypal-button.png',
		style : 'none',
		height : '30%',
		width : '80%',
		left : '10%',
		right : '10%'
	});

	// Listen for click events.
	paypalbutton.addEventListener('click', function() {
		var session_id = Ti.App.Properties.getString('session_id_');

		if (session_id == null) {
			if (rows == 0) {
				alert('Please Add Product');
			} else {
				Ti.App.Properties.setBool('cart_screen', true);
				Allview.remove(AllCart);
				var app = Ti.UI.createWindow({
					backgroundColor : 'white',
					url : '/ui/common/Checkout_Paypal.js',
					navBarHidden : true,
					fullscreen : true,
					modal:true

				});
				app.open();

			}

		} else {
			if (rows == 0) {
				alert('Please Add Product');
				Allview.remove(AllCart);
			} else {

			Allview.remove(AllCart);
			var app = Ti.UI.createWindow({
			backgroundColor : 'white',
			url : '/ui/common/Already_Login_Billing',
			fullscreen : true,
			navBarHidden : true,
			modal:true

		});
		app.open();
		}
}
	});

	// Add to the parent view.
	bottomCartProductView.add(paypalbutton);
	
	

	return Allview;
}

module.exports = Cart;

