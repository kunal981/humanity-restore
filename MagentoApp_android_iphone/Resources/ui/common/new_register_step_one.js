var register_ = Ti.UI.currentWindow;

var view = Ti.UI.createView({
	width : '100%',
	height : '100%',
});

register_.add(view);

var topView = Ti.UI.createView({
	backgroundImage : '/images/header_bg.png',
	backgroundColor : '#5F6465',
	height : '10%',
	width : '100%',
	top : 0
});

view.add(topView);



// Create a Button.
var aButton = Ti.UI.createButton({
	title : '  Back',
	backgroundImage : '/images/back.png',
	font : {
		fontSize : 14,
		fontWeight : 'bold',
	},
	color : 'white',

	height : '55%',
	width : '17%',
	left : '0.8%',
});

aButton.addEventListener('touchstart', function(e) {

		aButton.backgroundImage = '/images/back_hover.png';

	});
	aButton.addEventListener('touchcancel', function(e) {

		aButton.backgroundImage = '/images/back.png';

	});
	aButton.addEventListener('touchend', function(e) {

		aButton.backgroundImage = '/images/back.png';

	});
aButton.addEventListener('click', function() {
	    Ti.App.Properties.setString('email_register', null);
		Ti.App.Properties.setString('firstname_register', null);
		Ti.App.Properties.setString('lastname_register', null);
		Ti.App.Properties.setString('password_register', null);
	var Home = require('/ui/common/Sales');
    var home = new Home();
view.add(home);

});
topView.add(aButton);

// Create a Label.
var aLabel = Ti.UI.createLabel({
	text : 'STEP 1:Personal Information',
	color : '#ffffff',
	font : {
		fontSize : 15
	},
	height : 'auto',
	width : 'auto',
	

	textAlign : 'center'
});

// Add to the parent view.
topView.add(aLabel);

var view_scroll = Ti.UI.createScrollView({
	width : '100%',
	height : '90%',
	top:'10%'
});

view.add(view_scroll); 

// Create a TextField.
var aTextField_email = Ti.UI.createTextField({
	height : '8%',
	top : '5%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'email',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_email);

// Create a TextField.
var aTextField_password = Ti.UI.createTextField({
	height : '8%',
	top : '17%',
	left : '5%',
	width : '90%',
	right : '5%',
	passwordMask : true,
	hintText : 'password',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_password);
// Create a TextField.
var aTextField_conformpassword = Ti.UI.createTextField({
	height : '8%',
	top : '29%',
	left : '5%',
	width : '90%',
	right : '5%',
	passwordMask : true,
	hintText : 'conform password',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_conformpassword);

// Create a TextField.
var aTextField_firstname = Ti.UI.createTextField({
	height : '8%',
	top : '41%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'firstname',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_firstname);

// Create a TextField.
var aTextField_lastname = Ti.UI.createTextField({
	height : '8%',
	top : '53%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'lastname',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_lastname);


var aButton_register = Ti.UI.createButton({
	backgroundImage : '/images/backimage_button.png',
	style : 'none',
	font : {
		fontSize : 15,
		fontWeight : 'bold',
	},
	color : 'white',
	title : 'Continue',
	height : '8%',

	top : '65%',

	width : '40%',
	right : '10%',

});
aButton_register.addEventListener('touchstart', function(e) {

		aButton_register.backgroundImage = '/images/backimage_hover_button.png';

	});
	aButton_register.addEventListener('touchcancel', function(e) {

		aButton_register.backgroundImage = '/images/backimage_button.png';

	});
	aButton_register.addEventListener('touchend', function(e) {

		aButton_register.backgroundImage = '/images/backimage_button.png';

	});


//   register work*******************************start work************++++++++++++++++++++++++++++++++++++++++++++++++++=============
// Listen for click events.
aButton_register.addEventListener('click', function() {

	if (aTextField_email.value.length == '') {
		alert('please fill email');
	} else if (aTextField_password.value.length == '') {
		alert('please fill password');
	} else if (aTextField_conformpassword.value.length == '') {
		alert('please fill conformpassword');
	} else if (aTextField_firstname.value.length == '') {
		alert('please fill firstname');
	} else if (aTextField_lastname.value.length == '') {
		alert('please fill lastname');
	} else if (aTextField_email.value.length == '' && aTextField_password.value.length == '' && aTextField_conformpassword.value.length == '' && aTextField_firstname.value.length == '' && aTextField_lastname.value.length == '') {
		alert('please fill...');
	} else {
		if (aTextField_password.value == aTextField_conformpassword.value) {
			Ti.App.Properties.setString('email_register', aTextField_email.value);
			Ti.App.Properties.setString('firstname_register', aTextField_firstname.value);
			Ti.App.Properties.setString('lastname_register', aTextField_lastname.value);
			Ti.App.Properties.setString('password_register', aTextField_password.value);
			Ti.API.info('open');
			var app = Ti.UI.createWindow({
				backgroundColor : 'white',
				url : '/ui/common/new_register_step_second.js',
				navBarHidden : true,
				fullscreen : true,
				modal : true,
				//exitOnClose:true

			});
			app.open();
			Ti.API.info('next');
		} else {
			alert('not match password');
		}
	}

});
aButton_register.addEventListener('touchstart', function(e) {

	aButton_register.backgroundImage = '/images/backimage_hover_button.png';

});
aButton_register.addEventListener('touchcancel', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});
aButton_register.addEventListener('touchend', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});

// Add to the parent view.
view_scroll.add(aButton_register);
