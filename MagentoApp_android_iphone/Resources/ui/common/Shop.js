function Shop() {

	var font1 = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
	var font2 = (Titanium.Platform.displayCaps.platformHeight * 2.5) / 100;
	var font3 = (Titanium.Platform.displayCaps.platformHeight * 2.2) / 100;
	var font4 = (Titanium.Platform.displayCaps.platformHeight * 1.8) / 100;
	var font5 = (Titanium.Platform.displayCaps.platformHeight * 1.5) / 100;

	var border = (Titanium.Platform.displayCaps.platformHeight * 0.5) / 100;
	var border2 = (Titanium.Platform.displayCaps.platformHeight * 0.8) / 100;

	var hgt = Ti.Platform.displayCaps.platformHeight;
	var wdh = Ti.Platform.displayCaps.platformWidth;

	var Allview = Ti.UI.createView({
		backgroundImage : '/images/magento.png',
		top : 0,
		height : '100%',
		width : '100%',
		animated : false,
		transition : Titanium.UI.iPhone.AnimationStyle.NONE,

	});

	var indicatorView = Ti.UI.createView({
		backgroundColor : 'black',
		height : '25%',
		width : '40%',
		opacity : 0.7,
		borderRadius : 10
	});
// change android/iphone
	var activityIndicator = Ti.UI.createActivityIndicator({
		style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,

	});
	indicatorView.add(activityIndicator);

	var topView = Ti.UI.createView({
		backgroundImage : '/images/header_bg.png',
		backgroundColor : '#5F6465',
		height : '10%',
		width : '100%',
		top : 0
	});

	Allview.add(topView);

	// Create an ImageView.
	var iconTop = Ti.UI.createImageView({
		image : '/images/icon.png',
		width : '15%',
		height : '80%',
		left : '42%'
	});
	iconTop.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});

	// Add to the parent view.
	topView.add(iconTop);

	first();

	function first() {

		var body = Ti.UI.createView({
			backgroundImage : '/images/magento.png',
			width : '100%',
			height : '90%',
			top : '10%'
		});
		Allview.add(body);
		Allview.add(indicatorView);
		activityIndicator.show();

		var tableData = [];

		var colorSetIndex = 0;
		var cellIndex = 0;
		var thisView = [];
		var _name_ = [];
		var _image_url_ = [];
		var _id_ = [];
		var _sub_category_name = [];
		var _sub_category_url = [];
		var _sub_category_id = [];

		var url1 = "http://siliwi.com/index.php/apiinfo/index/categorylisting";
		var xhr = Ti.Network.createHTTPClient({
			onload : function(e) {

				var json = JSON.parse(this.responseText);

				for (var i = 0; i < json.result.length; i++) {
					_name_.push(json.result[i].name);
					_id_.push(json.result[i].Id);
					_image_url_.push(json.result[i].image_url);
					var level = json.result[i].level1;
					for (var j = 0; j < level.length; j++) {
						_sub_category_name.push(level[j].name);
						_sub_category_id.push(level[j].Id);
						_sub_category_url.push(level[j].url);
					}
				}
				//Ti.API.error("_sub_category_url========"+_sub_category_url);
				//Ti.API.error("_name_========"+_name_);
				//Ti.API.error("_image_url_========"+_image_url_);
				//Ti.API.error("_sub_category_name========"+_sub_category_name);
				//Ti.API.error("_sub_category_id========"+_sub_category_id);

				Ti.API.error("_name_.length========" + _name_.length);
				Ti.API.error("_name_========" + _name_);
				var length = _name_.length;
				Ti.API.error("length=======" + length);
				var div = parseInt(length / 3);
				Ti.API.error("div=======" + div);
				var mdule = length % 3;
				Ti.API.error("mdule=======" + mdule);
				var xloop = '';
				var Loop = '';
				if (mdule == 1) {
					Loop = parseInt(div + 1);
					xloop = 1;
				} else if (mdule == 2) {
					Loop = parseInt(div + 1);
					xloop = 2;
				} else {
					Loop = parseInt(div);
					xloop = 3;
				}

				for (var y = 0; y < Loop; y++) {
					var thisRow = Ti.UI.createTableViewRow({
						layout : "horizontal",
						height : hgt * 0.25,
						backgroundSelectedColor : 'transparent',
						selectedBackgroundColor : 'transparent',
						selectedColor : 'transparent',

					});
					for (var x = 0; x < 3; x++) {
						//alert(colorSetIndex);
						thisView[colorSetIndex] = Ti.UI.createView({
							backgroundColor : 'white',
							left : wdh * 0.05,
							height : hgt * 0.23,
							width : wdh * 0.27,

							backgroundFocusedColor : '#E89F4F',
							focusable : true,
							backgroundSelectedColor : '#E89F4F',

							id : colorSetIndex + 1
						});
						thisView[colorSetIndex].addEventListener('touchstart', function(e) {
							//	e.source.setBackgroundColor('#c60000');
							//alert(colorSetIndex);
							thisView[(e.source.id - 1)].setBackgroundColor('#E89F4F');
							// just for effect
						});
						thisView[colorSetIndex].addEventListener('touchcancel', function(e) {
							//	e.source.setBackgroundColor('white');
							thisView[(e.source.id - 1)].setBackgroundColor('white');
							// just for effect
						});
						thisView[colorSetIndex].addEventListener('touchend', function(e) {
							//	e.source.setBackgroundColor('white');
							thisView[(e.source.id - 1)].setBackgroundColor('white');
							// just for effect
						});
						thisView[colorSetIndex].addEventListener('click', function(e) {
							var temp = e.source.id;
							Ti.API.error("temp========" + temp);
							Ti.API.error("_sub_category_name========" + _sub_category_name);
							Ti.API.error("_sub_category_id========" + _sub_category_id);

							var SubCategory = require('/ui/common/subCategory');
							var subCategory = new SubCategory(_sub_category_name, _sub_category_id);
							Ti.UI.currentWindow.add(subCategory);
							//Ti.UI.currentWindow.add(subCategory);
							//Allview.add(subCategory);

						});

						if (_name_[colorSetIndex] != undefined) {
							thisRow.add(thisView[colorSetIndex]);
						}

						// Create an ImageView.
						var smallimage1 = Ti.UI.createImageView({
							image : _image_url_[colorSetIndex],
							width : '80%',
							height : '70%',
							bottom : '23%',
							backgroundSelectedColor : '#E89F4F',

							id : colorSetIndex + 1
						});

						// Add to the parent view.
						thisView[colorSetIndex].add(smallimage1);

						var Button1 = Ti.UI.createButton({
							backgroundImage : '/images/backimage_button.png',
							title : _name_[colorSetIndex],
							height : '15%',
							color : 'white',
							backgroundSelectedColor : '#E89F4F',

							font : {
								fontSize : font4
							},
							width : '90%',
							bottom : '4%',
							left : '5%',
							borderRadius : border,
							id : colorSetIndex + 1
						});

						// Listen for click events.
						Button1.addEventListener('click', function(e) {

						});

						// Add to the parent view.
						thisView[colorSetIndex].add(Button1);
						cellIndex++;
						colorSetIndex++;
						/*
						 if (colorSetIndex === _name_.length) {
						 colorSetIndex = 0;
						 }*/

					}
					tableData.push(thisRow);
				}
				var tableview = Ti.UI.createTableView({
					separatorColor : 'transparent',
					data : tableData,
					backgroundColor : 'transparent',
				});

				tableview.addEventListener("click", function(e) {
					if (e.source.objName) {
						Ti.API.info("---> " + e.source.objName + e.source.objIndex + " was clicked!");
					}
				});

				body.add(tableview);

				activityIndicator.hide();
				Allview.remove(indicatorView);

			},
			onerror : function(e) {
				activityIndicator.hide();
				Allview.remove(indicatorView);
				Ti.API.debug(e.error);
				alert('Internet Connection low');
			},
			timeout : 25000
		});

		xhr.open("GET", url1);
		xhr.send();
	}

	//*************************************************************************Bottom Tabs************************************************************************

	var bottomview = Ti.UI.createView({
		backgroundImage : '/images/footer_bg.png',
		backgroundColor : '#363D40',
		bottom : 0,
		height : '10%',
		width : '100%',
		layout : 'horizontal'
	});
	Allview.add(bottomview);

	var HomeTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	HomeTab.addEventListener('click', function() {
		var Home = require('/ui/common/Home');
		var home = new Home();
		Allview.add(home);
		homeicon.image = '/images/home_hover.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = 'white';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(HomeTab);

	// Create an ImageView.
	var homeicon = Ti.UI.createImageView({
		image : '/images/home.png',
		width : '40%',
		height : '50%',
		top : '13%'
	});

	// Add to the parent view.
	HomeTab.add(homeicon);

	// Create a Label.
	var Homelabel = Ti.UI.createLabel({
		text : 'HOME',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '0%',
		textAlign : 'center'
	});

	// Add to the parent view.
	HomeTab.add(Homelabel);

	var AccountTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	AccountTab.addEventListener('click', function() {
		var Account = require('/ui/common/Sales');
		var account = new Account();
		Allview.add(account);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user_hover.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = 'white';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';

	});
	bottomview.add(AccountTab);

	// Create an ImageView.
	var Accounticon = Ti.UI.createImageView({
		image : '/images/user.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	AccountTab.add(Accounticon);

	// Create a Label.
	var Accountlabel = Ti.UI.createLabel({
		text : 'ACCOUNT',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	AccountTab.add(Accountlabel);

	var ShopTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	ShopTab.addEventListener('click', function() {
		var Shop = require('/ui/common/Shop');
		var shop = new Shop();
		Allview.add(shop);

		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye_hover.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = 'white';
		Cartlabel.color = '#838383';
		Morelabel.color = '#838383';
	});
	bottomview.add(ShopTab);

	// Create an ImageView.
	var Shopicon = Ti.UI.createImageView({
		image : '/images/eye_hover.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	ShopTab.add(Shopicon);

	// Create a Label.
	var Shoplabel = Ti.UI.createLabel({
		text : 'SHOP',
		color : 'white',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	ShopTab.add(Shoplabel);

	var CartTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	CartTab.addEventListener('click', function() {
		var Cart = require('/ui/common/Cart');
		var cart = new Cart();
		Allview.add(cart);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart_hover.png';
		Moreicon.image = '/images/more.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = 'white';
		Morelabel.color = '#838383';
	});
	bottomview.add(CartTab);

	// Create an ImageView.
	var Carticon = Ti.UI.createImageView({
		image : '/images/cart.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	CartTab.add(Carticon);

	// Create a Label.
	var Cartlabel = Ti.UI.createLabel({
		text : 'CART',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	CartTab.add(Cartlabel);

	var MoreTab = Ti.UI.createView({
		top : '4%',
		height : '92%',
		width : '20%',
		borderRadius : border
	});
	MoreTab.addEventListener('click', function() {
		var More = require('/ui/common/More');
		var more = new More();
		Allview.add(more);
		homeicon.image = '/images/home.png';
		Accounticon.image = '/images/user.png';
		Shopicon.image = '/images/eye.png';
		Carticon.image = '/images/cart.png';
		Moreicon.image = '/images/more_hover.png';
		Homelabel.color = '#838383';
		Accountlabel.color = '#838383';
		Shoplabel.color = '#838383';
		Cartlabel.color = '#838383';
		Morelabel.color = 'white';
	});
	bottomview.add(MoreTab);

	// Create an ImageView.
	var Moreicon = Ti.UI.createImageView({
		image : '/images/more.png',
		width : '40%',
		height : '50%',
		top : '15%'
	});

	// Add to the parent view.
	MoreTab.add(Moreicon);

	// Create a Label.
	var Morelabel = Ti.UI.createLabel({
		text : 'MORE',
		color : '#838383',
		font : {
			fontSize : font5
		},
		bottom : '2%',
		textAlign : 'center'
	});

	// Add to the parent view.
	MoreTab.add(Morelabel);

	//********************************************************************End Bottom Tab*************************************************************

	return Allview;
}

module.exports = Shop;

