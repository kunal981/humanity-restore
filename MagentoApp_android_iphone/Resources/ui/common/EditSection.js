function Edit_Section() {

	var hgt = Titanium.Platform.displayCaps.platformHeight;
	var wdh = Ti.Platform.displayCaps.platformWidth;
	var font1 = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
	var font2 = (Titanium.Platform.displayCaps.platformHeight * 2.2) / 100;
	var font3 = (Titanium.Platform.displayCaps.platformHeight * 2) / 100;
	var font4 = (Titanium.Platform.displayCaps.platformHeight * 2.5) / 100;
	var font5 = (Titanium.Platform.displayCaps.platformHeight * 1.5) / 100;
	var font6 = (Titanium.Platform.displayCaps.platformHeight * 1.8) / 100;

	var myDatabase;
	var _product_id = [];
	var name_product = [];
	var _quatity_ = [];
	var _price_ = [];
	var image_path = [];

	//myDatabase = Ti.Database.install('/mydata/ProductInformationSave', 'ProductInformationSave');
	myDatabase = Ti.Database.open('ProductInformationSave');
	var get_info_database = myDatabase.execute('SELECT * from product_details');

	while (get_info_database.isValidRow()) {
		Ti.API.error('Enter Allview');
		_product_id.push(get_info_database.fieldByName('_product_id'));
		_quatity_.push(get_info_database.fieldByName('_quatity'));
		_price_.push(get_info_database.fieldByName('_single_price'));
		image_path.push(get_info_database.fieldByName('_image_path'));
		name_product.push(get_info_database.fieldByName('name_product'));

		get_info_database.next();
	}

	get_info_database.close();
	myDatabase.close();

	Ti.API.error('_quatity_=====' + _quatity_);
	Ti.API.error('_price_=====' + _price_);
	Ti.API.error("_quatity_" + _quatity_);
	Ti.API.error("_price_" + _price_);

	var top_view;
	var shopbutton;
	var cartbutton;
	var update_cart;

	var temp_value;
	var prod = [];
	var text = [];
	var tableview;
	var colorSetIndex = 0;
	var cellIndex = 0;

	var repeatGetProductView = [];
	var EditText = [];
	var tableData = [];
	var thisRow = [];

	var Allview = Ti.UI.createView({
		top : 0,
		height : '100%',
		width : '100%'
	});

	top_view = Ti.UI.createView({

		backgroundImage : '/images/header_bg.png',

		height : '10%',
		width : '100%',
		top : '0%'

	});
	Allview.add(top_view);

	var indicatorView = Ti.UI.createView({
		backgroundColor : 'black',
		height : '25%',
		width : '40%',
		opacity : 0.7,

		borderRadius : 10
	});
	// change android/iphone
	var activityIndicator = Ti.UI.createActivityIndicator({
		style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,

	});
	indicatorView.add(activityIndicator);

	shopbutton = Ti.UI.createButton({
		backgroundImage : '/images/backimage_button.png',
		font : {
			fontSize : 15,
			fontWeight : 'bold',
		},
		color : 'white',

		height : '60%',
		width : '35%',
		style : 'none',

		title : 'Clear Cart',
		color : 'white',

		left : '2%',

	});

	// Listen for click events.
	shopbutton.addEventListener('click', function() {

		var myDatabase;
		//myDatabase = Ti.Database.install('/mydata/ProductInformationSave', 'ProductInformationSave');
		myDatabase = Ti.Database.open('ProductInformationSave');
		myDatabase.execute('DELETE FROM product_details');
		myDatabase.close();
		tableData = [];
		tableview.setData(tableData);

		Allview.add(indicatorView);
		activityIndicator.show();
		setTimeout(function() {

			activityIndicator.hide();
			Allview.remove(indicatorView);
			var Home = require('/ui/common/Cart');
			var home = new Home();
			Allview.add(home);
		}, 2000);

	});

	// Add to the parent view.
	top_view.add(shopbutton);

	update_cart = Ti.UI.createButton({
		title : 'Update Cart',
		backgroundImage : '/images/backimage_button.png',
		font : {
			fontSize : 15,
			fontWeight : 'bold',
		},
		color : 'white',

		height : '60%',
		width : '35%',
		right : '2%',

	});

	// Listen for click events.
	update_cart.addEventListener('click', function() {

		Allview.add(indicatorView);
		activityIndicator.show();
		setTimeout(function() {

			activityIndicator.hide();
			Allview.remove(indicatorView);
		}, 2000);

		for (var i = 0; i < prod.length; i++) {

			//myDatabase = Ti.Database.install('/mydata/ProductInformationSave', 'ProductInformationSave');
			myDatabase = Ti.Database.open('ProductInformationSave');
			myDatabase.execute('UPDATE product_details SET _quatity=? WHERE _product_id=?', text[i], prod[i]);
			myDatabase.close();

		}
		prod = [];
		text = [];
		Ti.API.error("prod.length" + prod.length);
		Ti.API.error("text.length" + text.length);

		var Home = require('/ui/common/Cart');
		var home = new Home();
		Allview.add(home);

	});
	top_view.add(update_cart);

	fetch_data();

	function fetch_data() {

		var center_view_ = Ti.UI.createView({

			backgroundColor : '#C3C3C3',
			width : '100%',
			height : '90%',
			top : '10%',

		});
		Allview.add(center_view_);

		// create  view==========================================*************************************************************

		for (var j = 0; j < name_product.length; j++) {

			thisRow[j] = Ti.UI.createTableViewRow({
				height : Ti.UI.SIZE,
				width : '100%',

			});
			var send = Ti.UI.createButton({
				style : Ti.UI.iPhone.SystemButtonStyle.DONE,
				title : 'DONE',
				id:j,
				borderColor : ''

			});
			send.addEventListener('click', function(e) {
				//alert('kmdx');
				
				EditText[e.source.id].enabled = false;
				EditText[e.source.id].enabled = true;
			});
			EditText[j] = Ti.UI.createTextField({
				value : _quatity_[j],
				height : 50,
				width : '12%',
				right : 5,

				font : {
					fontSize : font6,
				},

				editable : true,
				keyboardToolbar : [send],

				keyboardType : Titanium.UI.KEYBOARD_NUMBER_PAD,
				returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
				borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
				id : j
			});
			thisRow[j].add(EditText[j]);

			EditText[j].addEventListener('change', function(e) {
				temp_value = e.source.id;
				prod.push(_product_id[temp_value]);
				text.push(EditText[temp_value].value);

			});

			repeatGetProductView[j] = Ti.UI.createView({
				height : Ti.UI.SIZE,
				width : '80%',
				left : '0%',
				layout : 'horizontal'
			});
			thisRow[j].add(repeatGetProductView[j]);

			// Create an ImageView.
			var productImage = Ti.UI.createImageView({
				image : image_path[j],
				width : 100,
				height : 80,
				bottom : '2%',
				top : '1%',
				left : 4,
			});
			productImage.addEventListener('load', function() {
				Ti.API.info('Image loaded!');
			});

			// Add to the parent view.
			repeatGetProductView[j].add(productImage);

			var detailView = Ti.UI.createView({
				height : Ti.UI.SIZE,
				width : Ti.UI.SIZE,
				layout : 'vertical',

			});

			repeatGetProductView[j].add(detailView);

			// Create a Label.
			var productNamelabel = Ti.UI.createLabel({
				text : name_product[j],
				color : 'black',
				font : {
					fontSize : font2
				},
				left : 10,
				width : 'auto',
				height : 'auto'

			});

			// Add to the parent view.
			detailView.add(productNamelabel);

			// Create a Label.
			var unitpricelabel = Ti.UI.createLabel({
				text : 'unitprice: ' + _price_[j],
				color : 'black',
				font : {
					fontSize : font3
				},
				left : 10,
				width : 'auto',
				height : 'auto'
			});

			// Add to the parent view.
			detailView.add(unitpricelabel);

			// Create a TextField.

			tableData.push(thisRow[j]);
		}

		tableview = Ti.UI.createTableView({
			data : tableData,
			backgroundColor : 'transparent',
			separatorColor : 'transparent'

		});

		tableview.addEventListener("click", function(e) {
			if (e.source.objName) {
				Ti.API.info("---> " + e.source.objName + e.source.objIndex + " was clicked!");
			}
		});
		Ti.API.error('====tableview=**************************************************************');

		Ti.API.error('====tableview=**************************************************************' + tableData.length);
		center_view_.add(tableview);
	}

	return Allview;

}

module.exports = Edit_Section;
