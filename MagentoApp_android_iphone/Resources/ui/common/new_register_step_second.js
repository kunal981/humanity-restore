var register_second = Ti.UI.currentWindow;

var _check_;
var view = Ti.UI.createView({
	
	width : '100%',
	height : '100%',
	backgroundColor:'white'
});

register_second.add(view);

var indicatorView = Ti.UI.createView({
	backgroundColor : 'black',
	height : '25%',
	width : '40%',
	opacity : 0.7,
	borderRadius : 10
});
// change iphone/android
var activityIndicator = Ti.UI.createActivityIndicator({
	style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,

});
indicatorView.add(activityIndicator);

var topView = Ti.UI.createView({
	backgroundImage : '/images/header_bg.png',
	backgroundColor : '#5F6465',
	height : '10%',
	width : '100%',
	top : 0
});

view.add(topView);

// Create a Button.
var aButton = Ti.UI.createButton({
	title : '  Back',
	backgroundImage : '/images/back.png',
	font : {
		fontSize : 14,
		fontWeight : 'bold',
	},
	color : 'white',

	height : '55%',
	width : '17%',
	left : '0.8%',
});
aButton.addEventListener('touchstart', function(e) {

		aButton.backgroundImage = '/images/back_hover.png';

	});
	aButton.addEventListener('touchcancel', function(e) {

		aButton.backgroundImage = '/images/back.png';

	});
	aButton.addEventListener('touchend', function(e) {

		aButton.backgroundImage = '/images/back.png';

	});

aButton.addEventListener('click', function() {
	Ti.App.Properties.setString('email_register', null);
	Ti.App.Properties.setString('firstname_register', null);
	Ti.App.Properties.setString('lastname_register', null);
	Ti.App.Properties.setString('password_register', null);
	register_second.close();
	/*
	var app = Ti.UI.createWindow({
			backgroundColor : 'white',
			url : 'ui/common/new_register_step_one.js',
			navBarHidden : true,
			fullscreen : true,
	
		});
		app.open();*/
	

	//var Home = require('Sales');
	//var home = new Home();
	//view.add(home);
});
topView.add(aButton);

// Create a Label.
var aLabel = Ti.UI.createLabel({
	text : 'STEP 2:Billing Information',
	color : '#ffffff',
	font : {
		fontSize : 15
	},
	height : 'auto',
	width : 'auto',

	textAlign : 'center'
});

// Add to the parent view.
topView.add(aLabel);

var view_scroll = Ti.UI.createScrollView({
	width : '100%',
	height : '90%',
	top:'10%'
});

view.add(view_scroll); 

// Create a TextField.
var aTextField_street = Ti.UI.createTextField({
	height : '8%',
	top : '5%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'Street',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_street);

// Create a TextField.
var aTextField_city = Ti.UI.createTextField({
	height : '8%',
	top : '17%',
	left : '5%',
	width : '90%',
	right : '5%',

	hintText : 'City',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_city);
// Create a TextField.
var aTextField_country = Ti.UI.createTextField({
	height : '8%',
	top : '29%',
	left : '5%',
	width : '90%',
	right : '5%',

	hintText : 'Country',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_country);

// Create a TextField.
var aTextField_region = Ti.UI.createTextField({
	height : '8%',
	top : '41%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'region',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_region);

// Create a TextField.
var aTextField_postalcode = Ti.UI.createTextField({
	height : '8%',
	top : '53%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'postalcode',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_postalcode);

// Create a TextField.
var aTextField_telephone = Ti.UI.createTextField({
	height : '8%',
	top : '65%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'telephone',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_telephone);


var checkbox = Ti.UI.createSwitch({
	//style : Ti.UI.Android.SWITCH_STYLE_CHECKBOX,

	value : false,
	left : '6%',
	top : '75%',
	height : 30,
	width : 'auto'
});

checkbox.addEventListener("change", function(e) {
	Ti.API.info("The checkbox has been set to " + e.value);
	_check_ = e.value;

});
view_scroll.add(checkbox);

// Create a Label.
var aLabel = Ti.UI.createLabel({
	text : 'Shipping in Different Address',
	color : '#000000',
	font : {
		fontSize : 14
	},
	height : 'auto',
	width : 'auto',
	top : '75%',
	left : '25%',
	textAlign : 'center'
});

// Add to the parent view.
view_scroll.add(aLabel);

var aButton_register = Ti.UI.createButton({
	backgroundImage : '/images/backimage_button.png',
	style : 'none',
	font : {
		fontSize : 14,
		fontWeight : 'bold',
	},
	color : 'white',
	title : 'Continue',
	height : '8%',

	top : '80%',

	width : '40%',
	right : '5%',

});
aButton_register.addEventListener('touchstart', function(e) {

		aButton_register.backgroundImage = '/images/backimage_hover_button.png';

	});
	aButton_register.addEventListener('touchcancel', function(e) {

		aButton_register.backgroundImage = '/images/backimage_button.png';

	});
	aButton_register.addEventListener('touchend', function(e) {

		aButton_register.backgroundImage = '/images/backimage_button.png';

	});

//   register work*******************************start work************++++++++++++++++++++++++++++++++++++++++++++++++++=============
// Listen for click events.
aButton_register.addEventListener('click', function() {
	if (aTextField_street.value.length == '') {
		alert('please fill street...');
	} else if (aTextField_city.value.length == '') {
		alert('please fill city...');
	} else if (aTextField_country.value.length == '') {
		alert('please fill country...');
	} else if (aTextField_region.value.length == '') {
		alert('please fill region');
	} else if (aTextField_postalcode.value.length == '') {
		alert('please fill postal code...');
	} else if (aTextField_telephone.value.length == '') {
		alert('please fill telephone...');
	} else if (aTextField_street.value.length == '' && aTextField_city.value.length == '' && aTextField_country.value.length == '' && aTextField_region.value.length == '' && aTextField_postalcode.value.length == '' && aTextField_telephone.value.length == '') {
		alert('please fill all details');
	} else {
		if (_check_ == true) {
			Ti.App.Properties.setString('street_', aTextField_street.value);
			Ti.App.Properties.setString('country_', aTextField_country.value);
			Ti.App.Properties.setString('city_', aTextField_city.value);
			Ti.App.Properties.setString('postalcode_', aTextField_postalcode.value);
			Ti.App.Properties.setString('telephone_', aTextField_telephone.value);
			var app = Ti.UI.createWindow({
				backgroundColor : 'white',
				url : 'ui/common/new_register_step_third.js',
				navBarHidden : true,
				fullscreen : true,
				
			});
			app.open();
		} else {

			view.add(indicatorView);
			activityIndicator.show();
			var email = Ti.App.Properties.getString('email_register');
			var first = Ti.App.Properties.getString('firstname_register');
			var last = Ti.App.Properties.getString('lastname_register');
			var pass = Ti.App.Properties.getString('password_register');
			Ti.App.Properties.setString('email_register', null);
			Ti.App.Properties.setString('firstname_register', null);
			Ti.App.Properties.setString('lastname_register', null);
			Ti.App.Properties.setString('password_register', null);

			Ti.App.Properties.setString('street_', aTextField_street.value);
			Ti.App.Properties.setString('country_', aTextField_country.value);
			Ti.App.Properties.setString('city_', aTextField_city.value);
			Ti.App.Properties.setString('postalcode_', aTextField_postalcode.value);
			Ti.App.Properties.setString('telephone_', aTextField_telephone.value);
			var status;
			var _message_;
			//http://siliwi.com/index.php/apiinfo/index/createcustomer?email=brst1dev9@gmail.com&pwd=developer10&cpwd=developer10&firstname=brst&lastname=testing&street=test&city=test&postcode=145678&telephone=9876543210

			var url1 = "http://siliwi.com/index.php/apiinfo/index/createcustomer?email=" + email + "&pwd=" + pass + "&cpwd=" + pass + "&firstname=" + first + "&lastname=" + last + "&street=" + aTextField_street.value + "&city=" + aTextField_city.value + "&countryid" + aTextField_country.value + "&postcode=" + aTextField_postalcode.value + "&telephone=" + aTextField_telephone.value;
			var xhr = Ti.Network.createHTTPClient({
				onload : function(e) {

					Ti.API.debug(this.responseText);
					Ti.API.info(this.responseText);
					var json = JSON.parse(this.responseText);
					Ti.API.info(json);
					for (var i = 0; i < json.result.length; i++) {
						status = json.result[i].success;
						_message_ = json.result[i].message;
					}
					if (status == 'failure') {
						Ti.API.info(".........." + status);
						activityIndicator.hide();
						view.remove(indicatorView);
						alert(_message_);

					} else {
						activityIndicator.hide();
						view.remove(indicatorView);
						alert(_message_);
						//Ti.UI.currentWindow.remove(view);
						var Account = require('/ui/common/Sales');
						var account = new Account();
						view.add(account);

					}

				},
				onerror : function(e) {
					activityIndicator.hide();
					view.remove(indicatorView);
					Ti.API.debug(e.error);
					alert('Server not respond');
				},
				timeout : 25000
			});

			xhr.open("GET", url1);
			xhr.send();

		}
	}

});
aButton_register.addEventListener('touchstart', function(e) {

	aButton_register.backgroundImage = '/images/backimage_hover_button.png';

});
aButton_register.addEventListener('touchcancel', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});
aButton_register.addEventListener('touchend', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});

// Add to the parent view.
view_scroll.add(aButton_register);

Ti.API.info('last');
