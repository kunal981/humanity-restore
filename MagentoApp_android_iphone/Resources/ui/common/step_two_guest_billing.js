var guest_billing = Ti.UI.currentWindow;

var _check_;
var myDatabase;
var _product_id = [];
var _quatity_ = [];
var _price_ = [];
var product_string = '';
var qualitity_string;
//myDatabase = Ti.Database.install('/mydata/ProductInformationSave', 'ProductInformationSave');
myDatabase = Ti.Database.open('ProductInformationSave');
var get_info_database = myDatabase.execute('SELECT * from product_details');

while (get_info_database.isValidRow()) {
	Ti.API.error('enter while condition');
	_product_id.push(get_info_database.fieldByName('_product_id'));
	_quatity_.push(get_info_database.fieldByName('_quatity'));

	_price_.push(get_info_database.fieldByName('_single_price'));

	get_info_database.next();
}

get_info_database.close();
myDatabase.close();

var total_sum = 0;
for (var i = 0; i < _product_id.length; i++) {
	Ti.API.error("_quatity_[i]  " + _quatity_[i] + " _price_[i] " + _price_[i] + " i " + i);
	total_sum = total_sum + (_quatity_[i] * _price_[i]);

}

for (var j = 0; j < _product_id.length; j++) {

	if (j == 0) {
		product_string = _product_id[0];
	} else {
		product_string = product_string + "," + _product_id[j];
	}
	Ti.API.error("product_string[i]  " + product_string + " _product_id " + _product_id[j] + " j " + j);

}

for (var k = 0; k < _quatity_.length; k++) {

	if (k == 0) {

		qualitity_string = _quatity_[0];
	} else {
		qualitity_string = qualitity_string + "," + _quatity_[k];
	}

}
Ti.API.error("total_sum====" + total_sum);
Ti.API.error("qualitity_string===" + qualitity_string);
Ti.API.error("product_string===" + product_string);
Ti.API.error("_quatity_" + _quatity_);
Ti.API.error("_price_" + _price_);

var view = Ti.UI.createView({
	width : '100%',
	height : '100%',
});

guest_billing.add(view);
var indicatorView = Ti.UI.createView({
	backgroundColor : 'black',
	height : '25%',
	width : '40%',
	opacity : 0.7,
	borderRadius : 10
});

var activityIndicator = Ti.UI.createActivityIndicator({
	style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,

});
indicatorView.add(activityIndicator);

var topView = Ti.UI.createView({
	backgroundImage : '/images/header_bg.png',
	backgroundColor : '#5F6465',
	height : '10%',
	width : '100%',
	top : 0
});

view.add(topView);

// Create a Button.
var aButton = Ti.UI.createButton({
	title : ' Back',
	backgroundImage : '/images/back.png',
	font : {
		fontSize : 14,
		fontWeight : 'bold',
	},
	color : 'white',

	height : '55%',
	width : '17%',
	left : '0.8%',
});

aButton.addEventListener('touchstart', function(e) {

	aButton.backgroundImage = '/images/back_hover.png';

});
aButton.addEventListener('touchcancel', function(e) {

	aButton.backgroundImage = '/images/back.png';

});
aButton.addEventListener('touchend', function(e) {

	aButton.backgroundImage = '/images/back.png';

});
aButton.addEventListener('click', function() {

	/*
	 Ti.App.Properties.setString('email_guest', null);
	 Ti.App.Properties.setString('firstname_guest', null);
	 Ti.App.Properties.setString('lastname_guest', null);
	 var app = Ti.UI.createWindow({
	 backgroundColor : 'white',
	 url : 'ui/common/Checkout_Paypal.js',
	 navBarHidden : true,
	 fullscreen : true,

	 });
	 app.open();*/
	guest_billing.close();
});
topView.add(aButton);

// Create a Label.
var aLabel = Ti.UI.createLabel({
	text : 'STEP 2:Billing Information',
	color : 'white',
	font : {
		fontSize : 15
	},
	height : 'auto',
	width : 'auto',

	textAlign : 'center'
});

// Add to the parent view.
topView.add(aLabel);

var view_scroll = Ti.UI.createScrollView({
	width : '100%',
	height : '90%',
	top:'10%'
});

view.add(view_scroll);

// Create a TextField.
var aTextField_street = Ti.UI.createTextField({
	height : '8%',
	top : '5%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'Street',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_street);

// Create a TextField.
var aTextField_city = Ti.UI.createTextField({
	height : '8%',
	top : '17%',
	left : '5%',
	width : '90%',
	right : '5%',

	hintText : 'City',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_city);
// Create a TextField.
var aTextField_country = Ti.UI.createTextField({
	height : '8%',
	top : '29%',
	left : '5%',
	width : '90%',
	right : '5%',

	hintText : 'Country',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_country);

// Create a TextField.
var aTextField_postalcode = Ti.UI.createTextField({
	height : '8%',
	top : '41%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'postalcode',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_postalcode);

// Create a TextField.
var aTextField_telephone = Ti.UI.createTextField({
	height : '8%',
	top : '51%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'telephone',
	//softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view_scroll.add(aTextField_telephone);

var checkbox = Ti.UI.createSwitch({
	//style : Ti.UI.Android.SWITCH_STYLE_CHECKBOX,

	value : false,
	left : '6%',
	top : '65%',
	height : 30,
	width : 'auto'
});

checkbox.addEventListener("change", function(e) {
	Ti.API.info("The checkbox has been set to " + e.value);
	_check_ = e.value;

});
view_scroll.add(checkbox);

// Create a Label.
var aLabel = Ti.UI.createLabel({
	text : 'Shipping in Different Address',
	color : '#000000',
	font : {
		fontSize : 14
	},
	height : 'auto',
	width : 'auto',
	top : '65%',
	left : '22%',
	textAlign : 'center'
});

// Add to the parent view.
view_scroll.add(aLabel);

var aButton_register = Ti.UI.createButton({
	backgroundImage : '/images/backimage_button.png',
	style : 'none',
	font : {
		fontSize : 14,
		fontWeight : 'bold',
	},
	color : 'white',
	title : 'Continue',
	height : '8%',

	top : '70%',

	width : '40%',
	right : '5%',

});
aButton_register.addEventListener('touchstart', function(e) {

	aButton_register.backgroundImage = '/images/backimage_hover_button.png';

});
aButton_register.addEventListener('touchcancel', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});
aButton_register.addEventListener('touchend', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});

//   register work*******************************start work************++++++++++++++++++++++++++++++++++++++++++++++++++=============
// Listen for click events.
aButton_register.addEventListener('click', function() {
	if (aTextField_street.value.length == '') {
		alert('please fill street...');
	} else if (aTextField_city.value.length == '') {
		alert('please fill city...');
	} else if (aTextField_country.value.length == '') {
		alert('please fill country...');
	} else if (aTextField_postalcode.value.length == '') {
		alert('please fill postal code...');
	} else if (aTextField_telephone.value.length == '') {
		alert('please fill telephone');
	} else if (aTextField_street.value.length == '' && aTextField_city.value.length == '' && aTextField_country.value.length == '' && aTextField_postalcode.value.length == '' && aTextField_telephone.value.length == '') {
		alert('please fill all details');
	} else {
		if (_check_ == true) {
			var app = Ti.UI.createWindow({
				backgroundColor : 'white',
				url : '/ui/common/step_three_different_billing_guest_address_paypal.js',
				navBarHidden : true,
				fullscreen : true,
				modal:true

			});
			app.open();

		} else {

			Ti.App.Properties.setString('street_guest', aTextField_street.value);
			Ti.App.Properties.setString('city_guest', aTextField_city.value);
			Ti.App.Properties.setString('country_guest', aTextField_country.value);
			Ti.App.Properties.setString('postalcode_guest', aTextField_postalcode.value);
			Ti.App.Properties.setString('telephone_guest', aTextField_telephone.value);

			var app = Ti.UI.createWindow({
				backgroundColor : 'white',
				url : '/ui/common/guest_paypal.js',
				navBarHidden : true,
				fullscreen : true,
				modal:true

			});
			app.open();

		}
	}

});
aButton_register.addEventListener('touchstart', function(e) {

	aButton_register.backgroundImage = '/images/backimage_hover_button.png';

});
aButton_register.addEventListener('touchcancel', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});
aButton_register.addEventListener('touchend', function(e) {

	aButton_register.backgroundImage = '/images/backimage_button.png';

});

// Add to the parent view.
view_scroll.add(aButton_register);

