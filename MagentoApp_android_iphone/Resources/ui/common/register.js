var register = Ti.UI.currentWindow;
var ss = Ti.App.Properties.getBool('register_');
Ti.App.Properties.setBool('register_', false);

var view = Ti.UI.createView({
	width : '100%',
	height : '100%',
});

register.add(view);
var indicatorView = Ti.UI.createView({
	backgroundColor : 'black',
	height : '25%',
	width : '40%',
	opacity : 0.7,
	borderRadius : 10
});

var activityIndicator = Ti.UI.createActivityIndicator({
	style : Ti.UI.ActivityIndicatorStyle.BIG_DARK,

});
indicatorView.add(activityIndicator);
// Create a TextField.
var aTextField_email = Ti.UI.createTextField({
	height : '8%',
	top : '3%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'email',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view.add(aTextField_email);

// Create a TextField.
var aTextField_password = Ti.UI.createTextField({
	height : '8%',
	top : '12%',
	left : '5%',
	width : '90%',
	right : '5%',
	passwordMask : true,
	hintText : 'password',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view.add(aTextField_password);
// Create a TextField.
var aTextField_conformpassword = Ti.UI.createTextField({
	height : '8%',
	top : '22%',
	left : '5%',
	width : '90%',
	right : '5%',
	passwordMask : true,
	hintText : 'conform password',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view.add(aTextField_conformpassword);

// Create a TextField.
var aTextField_firstname = Ti.UI.createTextField({
	height : '8%',
	top : '31%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'firstname',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view.add(aTextField_firstname);

// Create a TextField.
var aTextField_lastname = Ti.UI.createTextField({
	height : '8%',
	top : '40%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'lastname',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view.add(aTextField_lastname);

// Create a TextField.
var aTextField_street = Ti.UI.createTextField({
	height : '8%',
	top : '49%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'street',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view.add(aTextField_street);

// Create a TextField.
var aTextField_city = Ti.UI.createTextField({
	height : '8%',
	top : '58%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'city',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Titanium.UI.KEYBOARD_NUMBER_PAD,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view.add(aTextField_city);

// Create a TextField.
var aTextField_postalcode = Ti.UI.createTextField({
	height : '8%',
	top : '67%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'postalcode',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Titanium.UI.KEYBOARD_NUMBER_PAD,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view.add(aTextField_postalcode);

// Create a TextField.
var aTextField_telephone = Ti.UI.createTextField({
	height : '8%',
	top : '76%',
	left : '5%',
	width : '90%',
	right : '5%',
	hintText : 'telephone',
	softKeyboardOnFocus : Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS, // Android only
	keyboardType : Titanium.UI.KEYBOARD_NUMBER_PAD,
	returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
});

view.add(aTextField_telephone);

// Create a Button.
var aButton = Ti.UI.createButton({
	backgroundImage : '/images/backimage_button.png',
	style : 'none',
	font : {
		fontSize : 12,
		fontWeight : 'bold',
	},
	color : 'white',
	title : 'Register',
	height : '9%',
	top : '86%',
	left : '10%',
	width : '80%',
	right : '10%',
});

// Listen for click events.
aButton.addEventListener('click', function() {
	if (ss == true) {
//http://siliwi.com/index.php/apiinfo/index/transaction?custtype=register&emailid=tguess119@test.com&firstname=kamal&lastname=kant&street=panchkual&city=New%20York&region=NY&postcode=231234&country_id=us&telephone=1234567889&is_default_billing=kaml%20kianr%20mayidffh&prod-qty=2&prod-sku=98&storeid=35&pwd=kamal%20kant&transaction-type=checkmo		

var url1 = "http://siliwi.com/index.php/apiinfo/index/transaction?custtype=register&emailid=" + aTextField_email.value + "&firstname=" + aTextField_firstname.value + "&lastname=" + aTextField_lastname.value + "&street=" + aTextField_street.value + "&city=" + aTextField_city.value  + "&region=NY&postcode=" + aTextField_postalcode.value + "&telephone=" + aTextField_telephone.value+"&is_default_billing=kaml%20kianr%20mayidffh&prod-qty=2&prod-sku=98&storeid=35&pwd=kamal%20kant&transaction-type=checkmo";
				var xhr = Ti.Network.createHTTPClient({
					onload : function(e) {

						Ti.API.debug(this.responseText);
						Ti.API.info(this.responseText);
						var json = JSON.parse(this.responseText);
						Ti.API.info(json);
						for (var i = 0; i < json.result.length; i++) {
							status = json.result[i].success;
							_message_ = json.result[i].message;
						}
						if (status == 'failure') {
							Ti.API.info(".........." + status);
							activityIndicator.hide();
							view.remove(indicatorView);
							alert(_message_);

						} else {
							activityIndicator.hide();
							view.remove(indicatorView);
							alert(_message_);
							//Ti.UI.currentWindow.remove(view);
							var Account = require('Sales');
							var account = new Account();
							view.add(account);

						}

					},
					onerror : function(e) {
						activityIndicator.hide();
						view.remove(indicatorView);
						Ti.API.debug(e.error);
						alert('Internet Connection low');
					},
					timeout : 20000
				});

				xhr.open("GET", url1);
				xhr.send();

	} else {
		if (aTextField_email.value.length == '') {
			alert('please enter email..');
		} else if (aTextField_password.value.length == '') {
			alert('please enter password..');
		}
		else if(aTextField_conformpassword.value.length==''){
			alert('please enter conform password..');
		} else if (aTextField_firstname.value.length == '') {
			alert('please enter firstname..');
		} else if (aTextField_lastname.value.length == '') {
			alert('please enter lastname..');
		}   else if (aTextField_street.value.length == '') {
			alert('please enter street..');
		} else if (aTextField_city.value.length == '') {
			alert('please enter city..');
		} else if (aTextField_postalcode.value.length == '') {
			alert('please enter postalcode..');
		} else if (aTextField_telephone.value.length == '') {
			alert('please enter telephone no..');
		} else if (aTextField_email.value.length == '' && aTextField_password.value.length == '' && aTextField_conformpassword.value.length==''&& aTextField_firstname.value.length == '' && aTextField_lastname.value.length == '' && aTextField_street.value.length == ''  && aTextField_city.value.length == '' && aTextField_postalcode.value.length == '' && aTextField_telephone.value.length == '') {
			alert('please fill all things.....');
		} else {
			if ((aTextField_password.value==aTextField_conformpassword.value)) {
				view.add(indicatorView);
				activityIndicator.show();
				var status;
				var _message_;
//http://siliwi.com/index.php/apiinfo/index/createcustomer?email=brst1dev9@gmail.com&pwd=developer10&cpwd=developer10&firstname=brst&lastname=testing&street=test&city=test&postcode=145678&telephone=9876543210				
				var url1 = "http://siliwi.com/index.php/apiinfo/index/createcustomer?email=" + aTextField_email.value + "&pwd=" + aTextField_password.value +"&cpwd="+aTextField_conformpassword.value+ "&firstname=" + aTextField_firstname.value + "&lastname=" + aTextField_lastname.value + "&street=" + aTextField_street.value + "&city=" + aTextField_city.value  + "&postcode=" + aTextField_postalcode.value + "&telephone=" + aTextField_telephone.value;
				var xhr = Ti.Network.createHTTPClient({
					onload : function(e) {

						Ti.API.debug(this.responseText);
						Ti.API.info(this.responseText);
						var json = JSON.parse(this.responseText);
						Ti.API.info(json);
						for (var i = 0; i < json.result.length; i++) {
							status = json.result[i].success;
							_message_ = json.result[i].message;
						}
						if (status == 'failure') {
							Ti.API.info(".........." + status);
							activityIndicator.hide();
							view.remove(indicatorView);
							alert(_message_);

						} else {
							activityIndicator.hide();
							view.remove(indicatorView);
							alert(_message_);
							//Ti.UI.currentWindow.remove(view);
							var Account = require('Sales');
							var account = new Account();
							view.add(account);

						}

					},
					onerror : function(e) {
						activityIndicator.hide();
						view.remove(indicatorView);
						Ti.API.debug(e.error);
						alert('Internet Connection low');
					},
					timeout : 20000
				});

				xhr.open("GET", url1);
				xhr.send();
			}
			else{
			alert('not match password and conform password');
		}
		}
		

	}

});

aButton.addEventListener('touchstart', function(e) {

	aButton.backgroundImage = '/images/backimage_hover_button.png';

});
aButton.addEventListener('touchcancel', function(e) {

	aButton.backgroundImage = '/images/backimage_button.png';

});
aButton.addEventListener('touchend', function(e) {

	aButton.backgroundImage = '/images/backimage_button.png';

});

// Add to the parent view.
view.add(aButton);
register.open();
