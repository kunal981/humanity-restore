//############# in notify.js :
 
Ti.API.info("hello from a background service!");
 var db = Ti.Database.open('alarms');
 
var alertCount = 0;
var notification = null;
 
function notify(resp){
	// This creates the notification alert on a 'paused' app
	/*notification = Ti.App.iOS.scheduleLocalNotification({
		alertBody:resp,
		alertAction:"OK",
		userInfo:{"hello":"world"},
		badge:alertCount,
		date:new Date(new Date().getTime() + 10)
	});*/
}
 
 
function checkFeed()
{
 
        // silently ignore this if there's no network connection
	/*if (Titanium.Network.online == false) {
		return;
	}*/
	
	
	var next_alarm = db.execute('SELECT * FROM alarms WHERE is_next=1');
	if(next_alarm.rowCount>0)
	{
		while (next_alarm.isValidRow())
		{
		  var alarm_id = next_alarm.fieldByName('alarm_id');
		  var alarm_code = next_alarm.fieldByName('alarm_code');
		  var alarm_title = next_alarm.fieldByName('alarm_title');
		  var alarm_desc = next_alarm.fieldByName('alarm_desc');
		  var alarm_time = next_alarm.fieldByName('alarm_time');
			next_alarm.next();
		}
		var now_time=new Date();
	
		if((parseInt(alarm_time)-5000)<now_time.getTime() && (parseInt(alarm_time)+5000)>now_time.getTime())
		{
			db.execute("UPDATE alarms SET is_next=0 WHERE is_next=1");
			var rest_alarms = db.execute("SELECT * FROM alarms WHERE alarm_id>"+alarm_id+" AND alarm_time>'"+now_time.getTime()+"' LIMIT 1");
			if(rest_alarms.rowCount>0)
			{
				new_alarm_id=alarm_id;
				while (rest_alarms.isValidRow())
				{
				  var alarm_id = rest_alarms.fieldByName('alarm_id');
				  var alarm_code = rest_alarms.fieldByName('alarm_code');
				  var alarm_title = rest_alarms.fieldByName('alarm_title');
				  var alarm_desc = rest_alarms.fieldByName('alarm_desc');
				  var alarm_time = rest_alarms.fieldByName('alarm_time');
					rest_alarms.next();
				}
				db.execute("UPDATE alarms SET is_next=1 WHERE alarm_id="+alarm_id);
				
				if(OS_IOS)
				{
					if (Ti.Platform.name == "iPhone OS" && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {
					    Ti.App.iOS.registerUserNotificationSettings({
						    types: [
					            Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
					            Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
					            Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE,
					            Ti.App.iOS.USER_NOTIFICATION_TYPE_NONE
					        ]
					    });
					}
					var notification = Ti.App.iOS.scheduleLocalNotification({
					    alertAction: "OK",
					    alertBody: alarm_desc,
					    badge: 1,
					    date: new Date(parseInt(alarm_code)),
					    //sound: "/alert.wav",
					    userInfo: { "id":parseInt(alarm_code),"n_type":"actual"}
					}); 
					
				
					Ti.App.iOS.addEventListener('notification', function(e) {
					
					 	if (e.userInfo && "n_type" in e.userInfo){
					        checkFeed();
					    }
					 
					
					 	if (e.badge > 0) {
					        Ti.App.iOS.scheduleLocalNotification({
					            date: new Date(new Date().getTime()),
					            badge: -1,
					            userInfo: { "id":parseInt(alarm_dates[i]),"n_type":"cancel"}
					        });
					    }
					});
				}
				if(OS_ANDROID)
				{
					// Intent object to launch the application 
					/*
					var intent = Ti.Android.createIntent({
					    flags : Ti.Android.FLAG_ACTIVITY_CLEAR_TOP | Ti.Android.FLAG_ACTIVITY_NEW_TASK,
					    // Substitute the correct classname for your application
					    className : 'com.brst.com.humanityrestore.humanity',
					});
					intent.addCategory(Ti.Android.CATEGORY_LAUNCHER);
					
					// Create a PendingIntent to tie together the Activity and Intent
					var pending = Titanium.Android.createPendingIntent({
					    intent: intent,
					    flags: Titanium.Android.FLAG_UPDATE_CURRENT
					});*/
					
					// Create the notification
					/*var notification = Titanium.Android.createNotification({
					    // icon is passed as an Android resource ID -- see Ti.App.Android.R.
					    icon: Ti.App.Android.R.drawable.my_icon,
					    contentTitle: alarm_title,
					    contentText : alarm_desc,
					    //contentIntent: pending,
					    when : new Date(parseInt(alarm_code)),
					    icon : Ti.App.Android.R.drawable.appicon, //notification icon
			        	defaults : Titanium.Android.DEFAULT_ALL,
					});
					// Send the notification.
					Titanium.Android.NotificationManager.notify(parseInt(alarm_code), notification);*/
				}
				
			}
			else
			{
				db.execute('DELETE FROM alarms');
			}
		}
	}
 
	var t = new Date().getTime();
	Ti.API.info('checking feed in bg.. '+t);
 
 
 
 
	/*var xhr = Titanium.Network.createHTTPClient();
	xhr.timeout = 1000000;
	xhr.onerror = function(e){
		Ti.API.info('IN ERROR ' + e.error);
	};
	xhr.onload = function(){
	
		/*
		// demo to increase the badge number...
		alertCount++;
 
		var response = this.responseText;		
		//Ti.API.info("the reply was: "+response);
		// open the notification
		notify(response);
	};
	
	xhr.open('GET','http://staging.brstdev.com/sevanow/web/');
	xhr.send();*/

 
}
 
/*Ti.App.iOS.addEventListener('notification',function(){
	Ti.API.info('background event received = '+notification);
	Ti.App.currentService.stop();
	Ti.App.currentService.unregister();
});
 */
// Kick off a timer to trigger a function called 'checkFeed' every 10 seconds (= 10000 ms)
var timer = setInterval(checkFeed, 5000);
 
//####### END notify.js