	if(OS_ANDROID)
	{
		var activity = Ti.Android.currentActivity;
		
		activity.addEventListener('resume',resumeFunction);
		activity.addEventListener("resumed", resumedFunction);
		activity.addEventListener("pause", pauseFunction);
		
		/*Ti.App.addEventListener('resume',resumeFunction);
		Ti.App.addEventListener('resumed',resumedFunction);
		Ti.App.addEventListener('pause',pauseFunction);*/
	}
	
	function resumeFunction(e){
		Ti.API.info("app is resuming from the background");
	}
	
	function resumedFunction(e){
		Ti.API.info("app has resumed from the background");
		// this will unregister the service if the user just opened the app
		// is: not via the notification 'OK' button..
		if(service!=null){
			service.stop();
			service.unregister();
		}
                Titanium.UI.iPhone.appBadge = null;
	}
	
	function pauseFunction(e){
		console.log("app was paused from the foreground");
		
		if(OS_IOS)
		{
			service = Ti.App.iOS.registerBackgroundService({url:'notify.js'});
			Ti.API.info("registered background service = "+service);
		}
		if(OS_ANDROID)
		{
			//var SECONDS = 5000;
		    // every 10 minutes
		    var intent = Titanium.Android.createServiceIntent({
		        url : 'notify.js'
		    });
		    //intent.putExtra('interval', SECONDS * 1000);
		    //in millimeter
		    var service = Titanium.Android.createService(intent);
		    service.start();
		}
	}