// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
/*  Kosso : March 12th 2011 
This the only way I managed to do this without the app crashing on resume.
Done slightly differently to the KS example, since they unregister the service and 
do not use a setInterval timer.
*/
 
//############  in app.js :
 
 
// test for iOS 4+
function isiOS4Plus(){
	if (Titanium.Platform.name == 'iPhone OS'){
		var version = Titanium.Platform.version.split(".");
		var major = parseInt(version[0]);
		// can only test this support on a 3.2+ device
		if (major >= 4){
			return true;
		}
	}
	return false;
}
 
if (isiOS4Plus()){
 
	var service;
	
	// Ti.App.iOS.addEventListener('notification',function(e){
	// You can use this event to pick up the info of the noticiation. 
	// Also to collect the 'userInfo' property data if any was set
	//		Ti.API.info("local notification received: "+JSON.stringify(e));
	//	});
	// fired when an app resumes from suspension
	
	if(OS_IOS)
	{
		Ti.App.addEventListener('resume',resumeFunction);
		Ti.App.addEventListener('resumed',resumedFunction);
		Ti.App.addEventListener('pause',pauseFunction);
	}
	
	function resumeFunction(e){
		Ti.API.info("app is resuming from the background");
	}
	
	function resumedFunction(e){
		Ti.API.info("app has resumed from the background");
		// this will unregister the service if the user just opened the app
		// is: not via the notification 'OK' button..
		if(service!=null){
			service.stop();
			service.unregister();
		}
                Titanium.UI.iPhone.appBadge = null;
	}
	
	function pauseFunction(e){
		console.log("app was paused from the foreground");
		
		if(OS_IOS)
		{
			service = Ti.App.iOS.registerBackgroundService({url:'notify.js'});
			Ti.API.info("registered background service = "+service);
		}
		/*
		if(OS_ANDROID)
		{
			//var SECONDS = 5000;
		    // every 10 minutes
		    var intent = Titanium.Android.createServiceIntent({
		        url : 'notify.js'
		    });
		    //intent.putExtra('interval', SECONDS * 1000);
		    //in millimeter
		    var service = Titanium.Android.createService(intent);
		    service.start();
		}*/
	}
	
}