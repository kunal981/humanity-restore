	//var Context = require('Context');
	var user_library = Alloy.createCollection('user_info');
	// The table name is the same as the collection_name value from the 'config.adapter' object. This may be different from the model name.
	var user_table = user_library.config.adapter.collection_name;
	
	var db_users = Ti.Database.open('users');
	//
	var loggedin_user = db_users.execute('SELECT * FROM '+user_table+" WHERE is_active=1");
	if(loggedin_user.rowCount>0)
	{
		var setalarm = Alloy.createController("setalarm", {}).getView();
		if (OS_IOS) {
			 $.container.hide();
			 $.navGroupWin.open();
			 $.navGroupWin.openWindow(setalarm);
		}
		if (OS_ANDROID) {
		 	setalarm.open();
		}
	}
	else
	{
		var base_url="http://beta.brstdev.com/humanity/web/index.php";
		var current_win_ref;
		var style;
		var deviceToken="";
		/*if (Ti.Platform.name === 'iPhone OS'){
			 style = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
		}
		else {
		 	 style = Ti.UI.ActivityIndicatorStyle.DARK;
		}
		var activityIndicator = Ti.UI.createActivityIndicator({
		  color: 'green',
		  font: {fontFamily:'Helvetica Neue', fontSize:26, fontWeight:'bold'},
		  message: 'Loading...',
		  style:style,
		  top:10,
		  left:10,
		  height:Ti.UI.SIZE,
		  width:Ti.UI.SIZE
		});*/
		var indicatorView = Ti.UI.createView({
			backgroundColor : 'black',
			height : '95%',
			width : '90%',
			opacity : 0.5,
			top:"-97.5%"
		});
		
		if (Ti.Platform.name === 'iPhone OS'){
			 style = Ti.UI.iPhone.ActivityIndicatorStyle.BIG_DARK;
		}
		else {
		 	 style = Ti.UI.ActivityIndicatorStyle.BIG_DARK;
		}
		var activityIndicator = Ti.UI.createActivityIndicator({
			style : style
		
		});
		indicatorView.add(activityIndicator);		
		$.container.add(indicatorView);
		// Require the module
		var Cloud = require('ti.cloud');
		
		
		
		if(OS_IOS) { 
		   $.navGroupWin.open();
		   current_win_ref=$.navGroupWin;
		   
		} 
		if (OS_ANDROID) {
		   $.container.open();
		   //$.referral_text.hide();
		   //$.referral_text.show();
		   current_win_ref=$.index;
		   	style = $.createStyle({
			   	orientationModes: [Ti.UI.PORTRAIT,Ti.UI.UPSIDE_PORTRAIT],
			});
			$.container.applyProperties(style);
		}

	
	var first_focus = true;
	$.referral_text.addEventListener('focus', function f(e){
	    if(first_focus){
	        first_focus = false;
	        $.referral_text.blur();
	    }else{
	        $.referral_text.removeEventListener('focus', f);
	    }
	});
	
	activityIndicator.show();
	if(OS_ANDROID)
	{
		var CloudPush = require('ti.cloudpush');
		CloudPush.debug = true;
		CloudPush.enabled = true;
		CloudPush.showTrayNotificationsWhenFocused = true;
		CloudPush.focusAppOnPush = false;
		
		Cloud.debug = true;
		
		CloudPush.retrieveDeviceToken({
			success : function deviceTokenSuccess(e) {
				deviceToken = e.deviceToken;
				//Ti.App.Properties.setString('devicetoken', deviceToken);

				Ti.API.info('????Device Token: ' + e.deviceToken);
				//alert(deviceToken);
				loginUser(deviceToken);
			},
			error : function deviceTokenError(e) {
				indicatorView.hide();
				alert('Failed to register for push! ' + e.error);
			}
		});

				// Process incoming push notifications
		CloudPush.addEventListener('callback', function (evt) {
		    //alert("Notification received: " + evt.payload);
		    var notif_data=JSON.parse(evt.payload);
			var ew1 = Ti.UI.createAlertDialog({
			    title:notif_data['android']['title'], message:notif_data['android']['alert'],
			    buttonNames:[(OS_ANDROID)?Ti.Android.currentActivity.getString(Ti.Android.R.string.ok):"OK"]
			});
			ew1.show();
		    
		});
	}
	
	if(OS_IOS)
	{
		var deviceToken = null;
		// Check if the device is running iOS 8 or later
		if (Ti.Platform.name == "iPhone OS" && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {
		 
		 // Wait for user settings to be registered before registering for push notifications
		    Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {
		 
		 // Remove event listener once registered for push notifications
		        Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush); 
		 
		        Ti.Network.registerForPushNotifications({
		            success: deviceTokenSuccess,
		            error: deviceTokenError,
		            callback: receivePush
		        });
		    });
		 
		 // Register notification types to use
		    Ti.App.iOS.registerUserNotificationSettings({
			    types: [
		            Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
		            Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
		            Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE
		        ]
		    });
		}
		 
		// For iOS 7 and earlier
		else {
		    Ti.Network.registerForPushNotifications({
		 // Specifies which notifications to receive
		        types: [
		            Ti.Network.NOTIFICATION_TYPE_BADGE,
		            Ti.Network.NOTIFICATION_TYPE_ALERT,
		            Ti.Network.NOTIFICATION_TYPE_SOUND
		        ],
		        success: deviceTokenSuccess,
		        error: deviceTokenError,
		        callback: receivePush
		    });
		}
		// Process incoming push notifications
		function receivePush(e) {
		    //alert('Received push: ' + JSON.stringify(e));
		    //var notif_data=JSON.parse(eve.payload);
		    //var notif_data=JSON.parse(evt.payload);
		    var notif_json=JSON.stringify(e);
		    console.log(notif_json);
		    var notif_data=JSON.parse(notif_json);
			var ew1 = Ti.UI.createAlertDialog({
			    title:notif_data['data']['title'], message:notif_data['data']['aps']['alert'],
			    buttonNames:[(OS_ANDROID)?Ti.Android.currentActivity.getString(Ti.Android.R.string.ok):"OK"]
			});
			ew1.show();
		}
		// Save the device token for subsequent API calls
		function deviceTokenSuccess(e) {
		    deviceToken = e.deviceToken;
		    console.log("IOS Device Token: "+deviceToken);
		    loginUser(deviceToken);
		}
		function deviceTokenError(e) {
		   indicatorView.hide();
		   alert('Failed to register for push notifications! ' + e.error);
		    
		}
	}
	
		$.logo_wrapper.height=Titanium.Platform.displayCaps.platformHeight*0.32;
		if(OS_IOS)
		{
			$.referral_text.height=Titanium.Platform.displayCaps.platformHeight*0.06;
			$.enter_button.height=Titanium.Platform.displayCaps.platformHeight*0.06;
			//$.skip_button.height=40;
		}
	}
	



//$.help_popup.show();
function openSetAlarm(e)
{
	if(e.source.is_ref=="0")
	{
		create_user("",deviceToken);
	}
	else
	{
		if($.referral_text.value!="")
		{
			create_user($.referral_text.value,deviceToken);
		}
		else
		{
			alert("Please enter refferal code or click on skip button.");
		}
	}
}

function openPopup()
{
	$.help_popup.show();
	//db_users.execute('DELETE FROM '+user_table);
}

function closePopup()
{
	$.help_popup.hide();
}

function create_user(refferal_id,device_id)
{
	var success=0;
	if(OS_IOS)
	{
		cos="ios";
	}
	else
	{
		cos="android";
	}
	var xhr = Ti.Network.createHTTPClient({
		onload: function() {
	 		json = JSON.parse(this.responseText);
	 		if(json['success']==1)
			{
				var success=1;
				db_users.execute('INSERT INTO '+user_table+" (user_id,device_id,referal_code,is_active) VALUES (NULL,'"+device_id+"','"+json["refferal_code"]+"',1)");
			    var setalarm = Alloy.createController("setalarm", {}).getView();
			    if (OS_IOS) {
			        $.navGroupWin.openWindow(setalarm);
			    }
			    if (OS_ANDROID) {
			    	style = $.createStyle({
					   		orientationModes: [Ti.UI.PORTRAIT,Ti.UI.UPSIDE_PORTRAIT]
						});
					setalarm.applyProperties(style);
				 	setalarm.open();
			    }				
	 		}
	 		else
	 		{
	 			alert(json["error"]); 			
	 		}
		},
	    onerror: function(e) {
	         Ti.API.debug(e.error);
	        console.log(e.error);
	        alert('Unknow error occured.');
	    },
	    timeout:30000
	});
	xhr.open('GET',base_url+"?r=site/create-user&device_id="+device_id+"&login_os="+cos+"&refferal_id="+refferal_id);

	xhr.send();

}

function loginUser(device_id){
	var deviceId=device_id;
 // Log in to ACS
    Cloud.Users.login({
        login: 'brstdev1',
        password: 'brstdev1'
    }, function (e) {
 if (e.success) {
            //alert('Login successful');
            subscribeToChannel(deviceId);
        } else {
            alert('Error:\n' +
                ((e.error && e.message) || JSON.stringify(e)));
                indicatorView.hide();
        }
    });
}


function subscribeToChannel (device_id) {
 // Subscribes the device to the 'news_alerts' channel
 // Specify the push type as either 'android' for Android or 'ios' for iOS
    Cloud.PushNotifications.subscribe({
        device_token: device_id,
        channel: 'news_alerts',
        type: Ti.Platform.name == 'android' ? 'android' : 'ios'
    }, function (e) {
 if (e.success) {
 			indicatorView.hide();
            //alert('Subscribed');
        } else {
        	indicatorView.hide();
            alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        }
    });
}
function unsubscribeToChannel (device_id) {
 // Unsubscribes the device from the 'test' channel
    Cloud.PushNotifications.unsubscribe({
        device_token: device_id,
        su_id:"5523e39a54add893d5fe33aa",
        channel: 'news_alerts',
    }, function (e) {
 if (e.success) {
           indicatorView.hide();
           alert('Unsubscribed');
        } else {
           indicatorView.hide();
           alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        }
    });
}