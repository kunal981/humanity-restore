exports.definition = {
	config: {
		columns: {
		    "user_id": "INTEGER PRIMARY KEY AUTOINCREMENT",
		    "device_id": "TEXT",
		    "referal_code": "TEXT",
		    "is_active": "INTEGER"
		},
		adapter: {
			"type": "sql",
			"collection_name": "user_info",
			"idAttribute": "user_id",
			"db_name": "users",
			"db_file": "users.sqlite"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};