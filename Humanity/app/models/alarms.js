exports.definition = {
	config: {
		columns: {
		    "alarm_id": "INTEGER PRIMARY KEY AUTOINCREMENT",
		    "alarm_code": "TEXT",
		    "alarm_title": "TEXT",
		    "alarm_desc": "TEXT",
		    "alarm_time": "TEXT",
		    "is_next": "INTEGER"
		},
		adapter: {
			"type": "sql",
			"collection_name": "alarms",
			"idAttribute": "alarm_id",
			"db_name": "alarms",
			"db_file": "alarms.sqlite"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};